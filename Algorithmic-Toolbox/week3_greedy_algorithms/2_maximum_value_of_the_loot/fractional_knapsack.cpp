#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>

using namespace std;

double get_optimal_value(int capacity, const vector<int>& weights, const vector<int>& values) {

  // Prepares a vector with indexes of the elements sorted by unit values

  vector<pair<int, double>> ordered_items;
  ordered_items.reserve(weights.size());
  for(int n = 0; n < weights.size(); n++){
    if(weights[n] > 0){
      double value_per_unit = values[n] / (double) weights[n];
      ordered_items.push_back(make_pair(n, value_per_unit));
    }
  }
  sort(ordered_items.begin(), ordered_items.end(),
    [](const pair<int, double>& a, const pair<int, double>& b) {
      return a.second > b.second;
    }
  );

  // Perform the knapsack fitting

  double value = 0.0;
  int available_weight = capacity;
  int ordered_item = 0;
  while(ordered_item < ordered_items.size() && available_weight > 0){
    int item = ordered_items[ordered_item].first;
    if(weights[item] <= available_weight){
      value += values[item];
      available_weight -= weights[item];
    } else {
      value += ordered_items[ordered_item].second * available_weight;
      break;
    }
    ordered_item++;
  }
  return value;
}

/* Input
  3 50    (number of items, knapsack max weight)
  60 20   (item1: value, weight)
  100 50  (item2: value, weight)
  120 30  (item3: value, weight)

  Out: 180.000
*/
void interactive(){
  int n;
  int capacity;
  cin >> n >> capacity;
  vector<int> values(n);
  vector<int> weights(n);
  for (int i = 0; i < n; i++) {
    cin >> values[i] >> weights[i];
  }

  double optimal_value = get_optimal_value(capacity, weights, values);

  cout.precision(10);
  cout << optimal_value << endl;
}

int main() {
  interactive();
  return 0;
}
