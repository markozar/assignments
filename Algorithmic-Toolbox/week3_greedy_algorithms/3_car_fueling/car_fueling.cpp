#include <iostream>
#include <vector>

using namespace std;

/*  dist: is the distance between A and B.
    tank: miles that the car can drive with a full tank
    stops: distances of the fuel stops. A .. stops[0] .. stops[n-1] .. B
  Input example:
    950 (dist)
    400 (tank capacity in km)
    4   (number of fuel station)
    200 375 550 750 (stops)
*/ 
int compute_min_refills(int dist, int tank, vector<int> & stops) {
    // write your code here
    return -1;
}

/* tank: capacity in km
   stops: n stops in km, where stops[0] is the starting city A and stops[n-1] is the destination city B
   We hve no dist here because dist is stops[n-1]
*/
int compute_min_refills2(int tank, vector<int> & stops) {
    int refills = 0, last_stop = 0, car_curr_stop = 0;
    while(car_curr_stop < stops.size()){
        while(car_curr_stop < stops.size() && (stops[car_curr_stop + 1] - stops[last_stop]) <= tank){
            car_curr_stop++;
        }
        if(car_curr_stop == last_stop){ 
            return -1; 
        }
        last_stop = car_curr_stop;
        if(car_curr_stop < stops.size() - 1) refills++; // If we stop at the last stop, we reached the destination, it is not a refill stop
    }
    // write your code here
    return refills;
}

void interactive(){
    int d = 0;
    cin >> d;
    int m = 0;
    cin >> m;
    int n = 0;
    cin >> n;

    vector<int> stops(n);
    for (size_t i = 0; i < n; ++i)
        cin >> stops.at(i);

    cout << compute_min_refills(d, m, stops) << "\n";
}

void interactive2(){
    int d = 0;
    cin >> d;
    int m = 0;
    cin >> m;
    int n = 0;
    cin >> n;

    vector<int> stops;
    stops.push_back(0); // starting position A
    int stop;
    for (size_t i = 0; i < n; ++i){
        cin >> stop;
        stops.push_back(stop);
    }
    stops.push_back(d); // destination position B

    cout << compute_min_refills2(m, stops) << endl;
}

int main() {
    interactive2();
    return 0;
}
