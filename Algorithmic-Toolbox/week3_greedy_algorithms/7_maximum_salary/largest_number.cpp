#include <algorithm>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

string concatenate(const vector<string>& numbers) {
  //write your code here
  stringstream ret;
  for (size_t i = 0; i < numbers.size(); i++) {
    ret << numbers[i];
  }
  return ret.str();
}

// Works only with 2 strings. It verifies the safe move.
string largest_naive_2(vector<string> numbers) {
  string a = numbers[0];
  string b = numbers[1];
  int ai = stoi(a + b);
  int bi = stoi(b + a);
  if(ai < bi){
    numbers[0] = b;
    numbers[1] = a;
  }
  return concatenate(numbers);
}

bool compare_numbers2(const string& a, const string& b){
  int ai = stoi(a + b);
  int bi = stoi(b + a);
  return ai > bi;
}

/* Bugs:
  Different result for:
    12, 122
    Naive: 12212, My: 12122
  Different result for:
    10 101
    Naive: 10110, My: 10101
*/
bool compare_numbers(const string& a, const string& b){
  int size_a = a.size();
  int size_b = b.size();
  int max_index = max(size_a, size_b);
  int ai, bi;
  for(int i = 0; i < max_index; i++){
    ai = i % size_a;
    bi = i % size_b;
    if(a[ai] > b[bi]) return true;
    if(a[ai] < b[bi]) return false;
  }
  return false;
}

string largest_number(vector<string> numbers) {
  sort(numbers.begin(), numbers.end(), compare_numbers2);
  return concatenate(numbers);
}

void stress_test(){
  vector<string> numbers;
  numbers.resize(2);
  for(int a = 1; a <= 1000; a++){
    for(int b = 1; b <= 1000; b++){
      numbers[0] = to_string(a);
      numbers[1] = to_string(b);
      string res1 = largest_naive_2(numbers);
      string res2 = largest_number(numbers);
      if(res1 != res2){
        cout << "Different result for:" << endl << a << " " << b << endl << "Naive: " << res1 << ", My: " << res2 << endl;
        return;
      }
    }
  }
  cout << "All passed!" << endl;
}

void interactive(){
  int n;
  cin >> n;
  vector<string> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    cin >> a[i];
  }
  // cout << largest_naive_2(a) << endl;
  cout << largest_number(a) << endl;
}


int main() {
  // stress_test();
  interactive();
}
