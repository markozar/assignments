#include <iostream>

using namespace std;

int get_change(int m) {
  //write your code here
  return m/10 + (m % 10)/5 + (m % 5);
}

void interactive(){
  int m;
  cin >> m;
  cout << get_change(m) << '\n';
}

int main() {
  interactive();
  return 0;
}
