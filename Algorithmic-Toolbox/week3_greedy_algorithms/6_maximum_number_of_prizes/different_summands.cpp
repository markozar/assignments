#include <iostream>
#include <vector>

using namespace std;

vector<int> optimal_summands(int n) {
  vector<int> summands;
  for(int i = 1; i <= n; i++){
    n -= i;
    if(n > i){
      summands.push_back(i);
    } else {
      summands.push_back(n + i);
      break;
    }
  }
  return summands;
}

// 1 <= n <= 10^9
void interactive(){
  int n;
  cin >> n;
  vector<int> summands = optimal_summands(n);
  cout << summands.size() << '\n';
  for (size_t i = 0; i < summands.size(); ++i) {
    cout << summands[i] << ' ';
  }
}

int main() {
  interactive();
  return 0;
}
