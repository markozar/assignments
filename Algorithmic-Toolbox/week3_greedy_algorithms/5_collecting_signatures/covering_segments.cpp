#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>

using namespace std;

struct Segment {
  int start, end;
  bool visited = false;
};

vector<int> optimal_points(vector<Segment> &segments) {
  vector<int> points;
  //write your code here
  // for (size_t i = 0; i < segments.size(); ++i) {
  //   points.push_back(segments[i].start);
  //   points.push_back(segments[i].end);
  // }
  sort(segments.begin(), segments.end(), [](const Segment& s1, const Segment& s2){
    return s1.end < s2.end;
  });
  // O(n^2), with 1 <= n <= 100 it is ok.
  for(int i = 0; i < segments.size(); i++){
    auto segment = segments[i];
    if(segment.visited) continue;
    segment.visited = true; // not strictly necessary, but for completenss
    points.push_back(segment.end);
    for(int j = i + 1; j < segments.size(); j++){
      if(segments[j].start <= segment.end) segments[j].visited = true;
    }
  }
  return points;
}

/*
  Input:
  3   (n of segments)
  1 3 (segment 1)
  2 5 (segment 2)
  3 6 (segment 3)

  1 <= n <= 100,  0 <= a,b <= 10^9
*/

void interactive(){
  int n;
  cin >> n;
  vector<Segment> segments(n);
  for (size_t i = 0; i < segments.size(); ++i) {
    cin >> segments[i].start >> segments[i].end;
  }
  vector<int> points = optimal_points(segments);
  cout << points.size() << "\n";
  for (size_t i = 0; i < points.size(); ++i) {
    cout << points[i] << " ";
  }
}

int main() {
  interactive();
  return 0;
}
