#include <iostream>
#include <vector>
#include <cstdlib>
#include <utility>

using namespace std;
using std::swap;

pair<int, int> partition_3(vector<int> &a, int l, int r) {
  pair<int, int> m;
  int x = a[l];
  m.first = m.second = l;
  for (int i = l + 1; i <= r; i++) {
    if (a[i] < x) {
      a[m.first] = a[i];
      m.first++;
      m.second++;
      a[i] = a[m.second];
      a[m.second] = x;
    } else if(a[i] == x){
      m.second++;
      swap(a[i], a[m.second]);
    }
  }
  return m;
}

int partition_2(vector<int> &a, int l, int r) {
  int x = a[l];
  int j = l;
  for (int i = l + 1; i <= r; i++) {
    if (a[i] <= x) {
      j++;
      swap(a[i], a[j]);
    }
  }
  swap(a[l], a[j]);
  return j;
}

void randomized_quick_sort_2(vector<int> &a, int l, int r) {
  if (l >= r) return;

  int k = l + rand() % (r - l + 1);
  swap(a[l], a[k]);
  int m = partition_2(a, l, r);
  randomized_quick_sort_2(a, l, m - 1);
  randomized_quick_sort_2(a, m + 1, r); 
}

void randomized_quick_sort_3(vector<int> &a, int l, int r) {
  if (l >= r) return;

  int k = l + rand() % (r - l + 1);
  swap(a[l], a[k]);
  
  auto m = partition_3(a, l, r);
  randomized_quick_sort_3(a, l, m.first - 1);
  randomized_quick_sort_3(a, m.second + 1, r);
}

void stress_test2(size_t n){
  vector<int> a(n, 2);
  vector<int> b(n, 2);
  cout << "Calculating qsort2..." << endl;
  randomized_quick_sort_2(a, 0, a.size() - 1);
  cout << "Calculating qsort3..." << endl;
  randomized_quick_sort_3(b, 0, b.size() - 1);
  for(int i = 0; i < a.size(); i++){
    if(a[i] != b[i]){
      cout << "Ordered arrays do not match at a[" << i << "] = " << a[i] << ", b[" << i << "] = " << b[i] << endl;
      cout << "Array:" << endl;
      for(auto e: a) cout << e << " ";
      return;
    }
  }
  cout << "Ordered arrays do match!" << endl;
}

void stress_test1(size_t n){
  int counter = 0, feedback_every_n = 500;
  while(true){
    vector<int> a(n);
    for(auto& e: a) e = rand() % 100;
    vector<int> b(a);
    randomized_quick_sort_2(a, 0, a.size() - 1);
    randomized_quick_sort_3(b, 0, b.size() - 1);
    for(int i = 0; i < a.size(); i++){
      if(a[i] != b[i]){
        cout << "Ordered arrays do not match at a[" << i << "] = " << a[i] << ", b[" << i << "] = " << b[i] << endl;
        cout << "Array:" << endl;
        for(auto e: a) cout << e << " ";
        return;
      }
    }
    if(++counter >= feedback_every_n){
      cout << counter << " tests were OK. Last test:" << endl;
      for(auto e: a) cout << e << " ";
      counter -= feedback_every_n;
    }
  }
}

/* Input:
  5           ( 1 <= n <= 10^5 )
  2 3 9 2 2   ( 1 <= a <= 10^9 )

   Output:
   Sorted elements
   2 2 2 3 9
*/
void interactive(){
  int n;
  cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    cin >> a[i];
  }
  randomized_quick_sort_3(a, 0, a.size() - 1);
  for (auto e: a) cout << e << " ";
}

int main() {
  // stress_test1(1000);
  // stress_test2(50000);
  interactive();
  return 0;
}
