#include <algorithm>
#include <iostream>
#include <vector>
#include <random>

using namespace std;

int get_majority_element(vector<int> &a, int left, int right) {
  if (left == right) return -1;
  if (left + 1 == right) return a[left];
  //write your code here
  return -1;
}

int majority_moore(const vector<int>& elements){
  size_t n = elements.size();
  if(n == 1) return -1;

  size_t ptr_candidate = 0;
  int count = 1;

  for(size_t i = 1; i < n; i++){
    if(elements[ptr_candidate] == elements[i]){
      count++;
    } else {
      count--;
      if(count <= 0){
        ptr_candidate = i;
        count = 1;
      }
    }
  }
  count = 0;
  int candidate = elements[ptr_candidate];
  for(auto e: elements){
    e == candidate && count++;
  }
  if(count > n/2) return candidate;
  return -1;
}

int majority_naive(const vector<int>& elements){
  size_t n = elements.size();
  if(n == 1) return -1;
  n = (n / 2) + 1;
  for(auto a: elements){
    size_t count = 0;
    for(auto b: elements){
      if(a == b){
        count++;
        if(count >= n) return a;
      }
    }
  }
  return -1;
}

void stress_test(){
  vector<int> nums;
  int counter = 0, feedback_every_n = 100000;
  while(true){
    nums.clear();
    int size = rand() % 10;
    for(int i = 0; i < size; i++){
      nums.push_back(rand() % 3);
    }
    int res1 = majority_naive(nums);
    int res2 = majority_moore(nums);
    if(res1 != res2){
      cout << "Different majorities!" << endl;
      cout << "Naive: " << res1 << ", Moore: " << res2 << endl;
      cout << "Sequence:" << endl << nums.size() << endl;
      for(auto e: nums) cout << e << " ";
      cout << endl;
    }
    if(++counter >= feedback_every_n){
      cout << counter << " tests were OK. Last test:" << endl;
      cout << "Naive: " << res1 << ", Moore: " << res2 << endl;
      counter -= feedback_every_n;
    }
  }
}

/* Input:
  1 <= n <= 10^5
  0 <= a <= 10^9

  Output:
  1 if the sequence contains a majority element
  0 if not
*/
void interactive(){
  int n;
  cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    cin >> a[i];
  }
  // std::cout << (get_majority_element(a, 0, a.size()) != -1) << endl;
  // std::cout << (majority_moore(a) != -1) << endl;
  
  // std::cout << majority_naive(a) << endl;
  std::cout << majority_moore(a) << endl;
}

int main() {
  // stress_test();
  interactive();
}
