#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <cmath>
#include <limits>
#include <algorithm>

using std::vector;
using std::string;
using std::pair;
using std::min;

struct point{
  int32_t x;  // is it faster int64_t = int32_t * int32_t OR int64_t = int64_t * int64_t ?
  int32_t y;
  bool part;  // to differentiate partitions while merging
};

void log_test(const vector<point>& points){
  // std::cout << "Writing test case to file..." << std::endl;
  // std::fstream error_test;
  // error_test.open("test_fail", std::ios::out);
  // if(error_test.is_open()){
  //   error_test << " Failed test:" << std::endl;
  //   error_test << points.size() << std::endl;
  //   for(const auto& p: points) error_test << p.x << " " << p.y << std::endl;
  //   error_test.flush();
  //   error_test.close(); 
  // } else {
  //   std::cout << "Can not write the test case to test_fail file" << std::endl;
  // }
  std::cout << "Failed test case: " << std::endl;
  std::cout << points.size() << std::endl;
  for(const auto& p: points) std::cout << p.x << " " << p.y << std::endl; 
}

double minimal_distance_naive(const vector<point>& points) {
  double min_dist = std::numeric_limits<double>::max();
  double dx, dy, dist;
  int n = points.size();
  for(size_t i = 0; i < n; i++){
    for(size_t j = i + 1; j < n; j++){
      dx = double(points[i].x - points[j].x);
      dy = double(points[i].y - points[j].y);
      dist = dx*dx + dy*dy;
      if(dist < min_dist) min_dist = dist;
    }
  }
  return sqrt(min_dist);
}
int64_t merge_partitions(vector<point>& points, size_t left, size_t half, size_t right, int64_t dist){
  vector<point> strip;
  int64_t dx, dy;
  int32_t center = points[half].x;
  size_t i = half;

  do {  // left partition has always at least 1 point (because half is included)
    dx = center - points[i].x;
    if(dx*dx >= dist) break;
    point& p = points[i];
    p.part = false;
    strip.push_back(p);
  } while(i-- > left);
  i = half + 1;
  do {  // right partition can have 0 points (because half is excluded)
    dx = points[i].x - center;
    if(dx*dx >= dist) break;
    point& p = points[i];
    p.part = true;
    strip.push_back(p);
  } while(i++ < right);
  if(i == half + 1) return dist;  // if there are no points in the right strip, then dist is the minimum. We catch also dist = 0 case here

  switch (strip.size()){
  case 1:   // we will never go here. If there is only 1 point it is in the left strip and the previous 'if' will return
    return dist;
    break;
  case 2:
    dx = int64_t(strip[0].x - strip[1].x);
    dy = int64_t(strip[0].y - strip[1].y);
    return min(dx*dx + dy*dy, dist);
    break;
  }

  // Maybe we can avoid sorting grouping the points in  dist x dist cells with the integer division operation.
  // For example, if dist == 5, all points from 0 to 4 will go in the first group, all points from 5 to 9 in the second
  // and so on. To avoid ordering the resulting cells maybe we can split the points in 2dist x 2dist grid in this way:
  // point.y / 2dist, and also
  // (point.y + dist) / 2dist
  // So we have 2dist grid cells that overlaps dist. We can then check every single grid cell independently.
  // Does it make sense?
  sort(strip.begin(), strip.end(), [](const point& p1, const point& p2){
    if(p1.y == p2.y) return p1.x < p2.x;
    return p1.y < p2.y;
  });
  size_t max_index = strip.size() -1;
  size_t span = 3;
  for(size_t i = 0; i < max_index; i++){
    if(span > max_index - i) span = max_index - i;
    for(size_t j = i + 1; j <= i + span; j++){
      point& pi = strip[i];
      point& pj = strip[j];
      if(pi.part != pj.part){
        dx = pi.x - pj.x;
        dy = pi.y - pj.y;
        dist = min(dx*dx + dy*dy, dist);
      }
    }
  }
  return dist;
}

int64_t recursive_minimal_distance(vector<point>& points, size_t left, size_t right){
  size_t num_points = right - left + 1;
  int64_t dx, dy;
  if(num_points <= 3){  // The base case will have 2 or 3 points. Not 1
    switch (num_points){
    case 1:
      std::cout << "We have a partition with only 1 point!" << std::endl;
      std::cout << "Left: " << left << ", Right: " << right << std::endl;
      for(const auto& p: points) std::cout << p.x << " " << p.y << " ";
      std::cout << std::endl;
      exit(1);
      break;
    case 2:
      dx = points[left].x - points[right].x;
      dy = points[left].y - points[right].y;
      return dx*dx + dy*dy;
      break;
    case 3:
      dx = points[left].x - points[left + 1].x;
      dy = points[left].y - points[left + 1].y;
      int64_t dist1 = dx*dx + dy*dy;
      dx = points[left].x - points[right].x;
      dy = points[left].y - points[right].y;
      int64_t dist2 = dx*dx + dy*dy;
      dx = points[left + 1].x - points[right].x;
      dy = points[left + 1].y - points[right].y;
      int64_t dist3 = dx*dx + dy*dy;
      return min(min(dist1, dist2), dist3);
      break;
    }
  }
  size_t half = left + ((right - left) >> 1);
  int64_t min_left = recursive_minimal_distance(points, left, half);
  int64_t min_right = recursive_minimal_distance(points, half + 1, right);
  int64_t min_dist = min(min_left, min_right);
  min_dist = merge_partitions(points, left, half, right, min_dist);
  return min_dist;
}

double minimal_distance_fast(vector<point>& points) {
  sort(points.begin(), points.end(), [](const point& p1, const point& p2){
    return p1.x < p2.x;
  });
  int64_t min_dist = recursive_minimal_distance(points, 0, points.size() - 1);
  return sqrt(min_dist);
}

void stress_test(size_t num_points, int32_t range){
  int32_t counter = 0, feedback_every_n = 1000000 / num_points;
  vector<point> points;
  std::cout << std::fixed << std::setprecision(4);
  int32_t range_middle = range >> 1;
  while(true){
    size_t np = 2 + rand() % (num_points -1);
    // points.clear();
    points.resize(np);
    for(auto& p: points){
      p.x = rand() % range - range_middle;
      p.y = rand() % range - range_middle;
    }
    double res1 = minimal_distance_naive(points);
    double res2 = minimal_distance_fast(points);
    // double res1 = res2;
    if(abs(res2 - res1) > 0.001){
      std::cout << "Different distances" << std::endl << "Naive: " << res1 << ", Fast: " << res2 << std::endl;
      log_test(points);
      return;
    }
    if(++counter >= feedback_every_n){
      std::cout << counter << " tests were OK. Last test:" << std::endl;
      std::cout << "Naive: " << res1 << ", Fast: " << res2 << std::endl;
      counter -= feedback_every_n;
    }
  }
}

/* Input:
    2     (n points 2 <= n <= 10^5)
    0 0   (x, y point 1  -10^9 <= x,y 10^9)
    3 4   (x, y point 2)

  Output
    5.0   (minimal distance 10^-3 precision)
*/
void interactive(){
  size_t n;
  std::cin >> n;
  vector<point> points(n);
  for (size_t i = 0; i < n; i++) {
    std::cin >> points[i].x >> points[i].y;
  }
  std::cout << std::fixed;
  // std::cout << std::setprecision(9) << minimal_distance_naive(points) << std::endl;
  std::cout << std::setprecision(9) << minimal_distance_fast(points) << std::endl;
}

int main() {
  // stress_test params: (size_t num_points, int32_t range)
  // stress_test(100, 2000000000);
  // stress_test(6, 10);
  interactive();
  return 0;
}
