#include <iostream>
#include <algorithm>
#include <random>
#include <cassert>
#include <vector>

using namespace std;

int binary_search(const vector<int> &a, int x) noexcept {
  int left = 0, right = (int)a.size() - 1; 
  while(left <= right){
    int mid = left + (right - left) / 2; // integer division (no need to floor)
    int val = a[mid];
    if(x == val) return mid;
    if(x < val){
      right = mid -1;
    } else {
      left = mid + 1;
    }
  }
  return -1;
}

int linear_search(const vector<int> &a, int x) noexcept {
  for (size_t i = 0; i < a.size(); ++i) {
    if (a[i] == x) return i;
  }
  return -1;
}

void stress_test(int array_size){
  int counter = 0, feedback_every_n = 100000;
  std::uniform_int_distribution<> rnd(0,1);
  std::default_random_engine dre;
  while(true){
    int as = 1 + rand() % (array_size - 1);
    vector<int> a;
    a.reserve(as);
    int number = 1;
    while(a.size() < as){
      // Fill a with randomly selected ascending numbers
      if(rnd(dre)) a.push_back(number);
      number++;
    }
    for(int i = 0; i < 1000; i++){
      int s = 1 + rand() % number;
      int r1 = linear_search(a, s);
      int r2 = binary_search(a, s);
      if(r1 !=r2){
        cout << "The 2 tests differ searching for " << s << ":" <<endl;
        cout << "Linear: " << r1 << ", Binary: " << r2 << endl;
        cout << "Array: " << endl << a.size() << " ";
        for(auto e : a) cout << e << " ";
        cout << endl;
        return;
      }
      if(++counter >= feedback_every_n){
        cout << counter << " tests were OK. Last test:" << endl;
        cout << "Linear: " << r1 << ", Binary: " << r2 << endl;
        counter -= feedback_every_n;
      }
    }
  }
}

/* Input:
    5 1 5 8 12 13   (n, a(0) .. a(n-1), where a is the array where are the alements to search for)
    5 8 1 23 1 11   (k, b(0) .. b(k-1), where b is the array of elemets to searh for in a)

    1 <= n <= 3*10^4
    1 <= k <= 10^5
    1 <= a(i), b(i) <= 10^9

  Output:
    if b(i) is in a, then return the index of b(i) in a, otherwise return -1. Output for the previus Input:
    2 0 -1 0 -1
*/
void interactive(){
  int n;
  cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    cin >> a[i];
  }
  int m;
  cin >> m;
  vector<int> b(m);
  for (int i = 0; i < m; ++i) {
    cin >> b[i];
  }
  // for (int i = 0; i < m; ++i) {
  //   cout << linear_search(a, b[i]) << ' ';
  // }
  // cout << endl;
  for (int i = 0; i < m; ++i) {
    cout << binary_search(a, b[i]) << ' ';
  }
}

int main() {
  // stress_test(10000);
  interactive();
  return 0;
}

/*
The 2 tests differ searching for 385:
Linear: -1, Binary: 195
Array: 
195 1 4 5 7 8 9 11 12 13 16 18 19 22 23 24 25 27 28 29 31 36 38 39 41 44 47 49 54 55 56 59 61 62 63 65 69 72 74 75 76 77 81 82 84 85 86 88 89 90 92 94 95 98 99 100 101 102 104 112 114 116 117 118 123 125 126 127 129 133 135 138 139 140 141 142 143 145 146 147 148 149 152 153 154 155 158 159 162 163 164 165 169 170 173 174 175 176 177 178 179 180 181 182 185 186 189 195 197 200 201 202 203 205 206 207 211 212 213 215 218 220 223 224 225 232 233 234 235 238 241 242 245 247 250 251 256 257 258 259 260 262 266 267 268 274 280 282 283 286 289 290 296 297 298 301 303 305 307 309 310 311 312 313 314 315 316 318 319 320 324 326 333 337 338 339 340 344 346 349 350 351 352 353 358 361 364 365 366 367 368 376 378 379 383 384
*/