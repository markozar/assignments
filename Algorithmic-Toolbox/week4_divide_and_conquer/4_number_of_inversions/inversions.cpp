#include <iostream>
#include <vector>

using namespace std;

int inversions_naive(vector<int>& a){
  if(a.size() < 2) return 0;
  int inversions = 0;
  for(int i = 0; i < a.size(); i++){
    for(int j = i + 1; j < a.size(); j++){
      if(a[i] > a[j]) inversions++;
    }
  }
  return inversions;
}

size_t merge(vector<int>& a, vector<int>& b, size_t left, size_t middle, size_t right){
  size_t s = left;
  size_t inversions = 0;
  size_t i = left;
  size_t j = middle + 1;

  while(i <= middle && j <= right){
    if(a[i] <= a[j]){
      b[s++] = a[i++];
    } else {
      b[s++] = a[j++];
      inversions += middle - i + 1;
    }
  }
  if(inversions > 0){
    if(i > middle){
      while(j <= right) b[s++] = a[j++];
    } else {
      while(i <= middle) b[s++] = a[i++];
    }
    for(i = left; i <= right; i++){
      a[i] = b[i];
    }
  }
  return inversions;
}

size_t get_merged_inversions(vector<int>& a, vector<int>& b, size_t left, size_t right){
  if(right <= left){
    // b[left] = a[left];
    return 0;
  }
  size_t half = left + (right - left) / 2;
  size_t inversions = get_merged_inversions(a, b, left, half);
  inversions += get_merged_inversions(a, b, half + 1, right);
  inversions += merge(a, b, left, half, right);

  return inversions;
}

size_t inversions_fast(vector<int>& a){
  vector<int> b(a.size());
  size_t inversions = get_merged_inversions(a, b, 0, a.size() - 1);
  return inversions;
}

void stress_test(size_t max_size){
  int counter = 0, feedback_every_n = 1000000 / max_size;
  int max_rnd = 10;
  while(true){
    size_t n = 1 + rand() % max_size;
    vector<int> a;
    a.reserve(n);
    for(int i = 0; i < n; i++){
      a.push_back(rand() % max_rnd);
    }
    max_rnd = max(1, (max_rnd + 1) % 10000);
    int res1 = inversions_naive(a);
    int res2 = inversions_fast(a);
    if(res1 != res2){
      cout << "Inversion count differs:"  << endl;
      cout << "Naive: " << res1 << ", Fast: " << res2 << endl << "Array: " << endl;
      for(auto e: a) cout << e << " ";
      return;
    }
    if(++counter >= feedback_every_n){
      cout << counter << " tests were OK. Last test:" << endl;
      cout << "Naive: " << res1 << ", Fast: " << res2 << endl << "Array: " << endl;
      for(auto e: a) cout << e << " ";
      cout << endl << endl;
      counter -= feedback_every_n;
    }
  }


}

/* Input:
    5           (1 <= n <= 30000)
    2 3 9 2 9   (1 <= a <= 10^9)
*/
void interactive(){
  int n;
  cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    cin >> a[i];
  }
  // cout << get_number_of_inversions(a, b, 0, a.size()) << endl;
  // cout << inversions_naive(a) << endl;
  cout << inversions_fast(a) << endl;
  // for(auto e: a) cout << e << " ";
}

int main() {
  // stress_test(1000);
  interactive();
  return 0;
}
