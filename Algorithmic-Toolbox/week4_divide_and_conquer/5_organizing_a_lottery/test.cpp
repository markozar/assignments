#include <iostream>
#include <fstream>

using namespace std;

int main(){
    ofstream error_test;
    error_test.open ("test_fail", ios::out);
    if(error_test.is_open()){
        error_test << 5 << " " << 4 << endl;
        error_test << "Hello World!" << endl;
        error_test.close(); 
        return 0;
    } else {
        cout << "Can not write the test case to test_fail file" << endl;
        return 1;
    }
}
