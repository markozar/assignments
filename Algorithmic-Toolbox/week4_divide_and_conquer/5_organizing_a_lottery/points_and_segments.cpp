#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <algorithm>
#include <exception>

using namespace std;
using seg_vector = vector<pair<int, int>>;
struct point {
  int x;
  int index;  // -1 for segments, otherwise the index in the points vector
  int info ;  // 1: segment start point, 2: lottery point, 3: segment end point
};

vector<int> fast_count_segments(const seg_vector& segments, const vector<int>& points) {
  vector<int> cnt(points.size());
  vector<point> all_points;
  all_points.reserve(segments.size() * 2 + points.size());
  for(int i = 0; i < points.size(); i++){
    all_points.push_back({points[i], i, 2});
  }
  for(int i = 0; i < segments.size(); i++){
    all_points.push_back({segments[i].first, -1, 1});
    all_points.push_back({segments[i].second, -1, 3});
  }
  sort(all_points.begin(), all_points.end(), [](const point& p1, const point& p2){
    if(p1.x == p2.x) return p1.info < p2.info;
    return p1.x < p2.x;
  });
  int counter = 0;
  for(auto p: all_points){
    switch (p.info){
    case 1:
      counter++;
      break;
    case 2:
      if(p.index >= cnt.size()){
        cout << "p.index >= cnt.size()" << endl;
        terminate();
      }
      cnt[p.index] = counter;
      break;
    case 3:
      counter--;
      break;    
    }
  }
  //write your code here
  return cnt;
}

vector<int> naive_count_segments(const seg_vector& segments, const vector<int>& points) {
  vector<int> cnt(points.size());
  for (size_t i = 0; i < points.size(); i++) {
    for (size_t j = 0; j < segments.size(); j++) {
      cnt[i] += segments[j].first <= points[i] && points[i] <= segments[j].second;
    }
  }
  return cnt;
}

void log_test(int ns, int np, const seg_vector& segments, const vector<int>& points){
  ofstream error_test;
  error_test.open ("test_fail", ios::out);
  if(error_test.is_open()){
    error_test << ns << " " << np << endl;
    for(auto s: segments) error_test << s.first << " " << s.second << endl;
    for(auto p: points) error_test << p << " ";
    error_test.close(); 
  } else {
    cout << "Can not write the test case to test_fail file" << endl;
  }
}

void stress_test(int max_segments, int max_points){
  int counter = 0, feedback_every_n = 1000000 / (max_points + max_segments * 2);
  int ns, np;
  seg_vector segments;
  vector<int> points;
  while(true){
    try{
      ns = 1 + rand() % max_segments;
      np = 1 + rand() % max_points;
      segments.resize(ns);
      points.resize(np);
      for(auto& s: segments){
        s.first = -(rand() % 2000);
        s.second = s.first + rand() % 2000;
      }
      for(auto& p: points){
        p = (rand() % 5000) - 2500;
      }
    } catch(exception& e){
      cout << "Exception allocating!";
      cout << e.what() << endl;
      log_test(ns, np, segments, points);
    }
    try{
      vector<int> cnt1 = naive_count_segments(segments, points);
      vector<int> cnt2 = fast_count_segments(segments, points);
      if(cnt1.size() != cnt2.size()){
        cout << "Different solution sizes:" << endl;
        cout << "Naive size: " << cnt1.size() << ", Fast size: " << cnt2.size() << endl;
        log_test(ns, np, segments, points);
        return;
      }
      for(int i = 0; i < cnt1.size(); i++){
        if(cnt1[i] != cnt2[i]){
          cout << "Different solutions:" << endl;
          cout << "Naive: ";
          for(auto c: cnt1) cout << c << " ";
          cout << endl << "Fast : ";
          for(auto c: cnt2) cout << c << " ";
          cout << endl;
          log_test(ns, np, segments, points); 
          return;
        }
      }
      if(++counter >= feedback_every_n){
        cout << counter << " tests were OK. Last test:" << endl;
        cout << "Naive: ";
        for(auto c: cnt1) cout << c << " ";
        cout << endl << "Fast : ";
        for(auto c: cnt2) cout << c << " ";
        cout << endl;
        counter -= feedback_every_n;
      }
    } catch (exception& e){
      cout << "Exception in counting!"<< endl;
      cout << e.what() << endl;
      log_test(ns, np, segments, points);
    }
  }
}

/* Input:
    2 3       (n segments, m points / 1<= n,m <= 50000)
    0 5       (a,b segment 1)
    7 10      (a,b segment 2)
    1 6 11    (x points)
    points and segments -10^8 <= a,b,x <= 10^8

  Output:
    1 0 0     (number of intersections for each x)
*/
void interactive(){
  int n, m;
  cin >> n >> m;

  seg_vector segments(n);
  for(auto& s: segments) cin >> s.first >> s.second;

  vector<int> points(m);
  for(auto& p: points) cin >> p;

  //use fast_count_segments
  // vector<int> cnt = naive_count_segments(segments, points);
  // for (auto c: cnt) cout << c << " ";
  // cout << endl;
  vector<int> cnt = fast_count_segments(segments, points);
  for (auto c: cnt) cout << c << " ";
  // cout << endl;
}

int main() {
  // stress_test(10, 10);
  interactive();
  return 0;
}
