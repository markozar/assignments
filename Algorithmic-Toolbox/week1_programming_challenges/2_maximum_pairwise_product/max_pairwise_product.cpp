#include <cstdlib>
#include <iostream>
#include <vector>
// #include <algorithm>

using namespace std;
using ull = unsigned long long;

ull MaxPairwiseProduct(const vector<ull>& numbers) {
    ull max_product = 0;
    int n = numbers.size();

    for (int first = 0; first < n; ++first) {
        for (int second = first + 1; second < n; ++second) {
            max_product = max(max_product,
                numbers[first] * numbers[second]);
        }
    }
    return max_product;
}

ull MaxPairwiseProductFast(const vector<ull>& numbers) {
    ull max_1 = 0, max_2 = 0;
    int n = numbers.size();

    for (int i = 0; i < n; ++i) {
        if(numbers[i] > max_1){
            max_2 = max_1;
            max_1 = numbers[i];
        } else if(numbers[i] > max_2){
            max_2 = numbers[i];
        }
    }
    return max_1 * max_2;
}

void print_test_case(const vector<ull>& numbers){
    cout << "Size " << numbers.size() << ", sequence:" << endl;
    for(int i = 0; i < numbers.size(); i++){
        cout << numbers[i] << " ";
    }
    cout << endl;
}

void StressTest(int size, int max_number){
    int counter = 0, feedback_every_n = 400000;
    while(true){
        int n = rand() % size + 2;
        vector<ull> numbers(n);
        for(int i = 0; i < n; i++){
            numbers[i] = rand() % max_number;
        }
        ull test1 = MaxPairwiseProduct(numbers);
        ull test2 = MaxPairwiseProductFast(numbers);
        if(test1 != test2){
            cout << "The 2 tests differ in this input:" << endl;
            print_test_case(numbers);
            break;
        }
        if(++counter >= feedback_every_n){
            cout << counter << " tests were OK. Last test:" << endl;
            print_test_case(numbers);
            counter -= feedback_every_n;
        }
    }
}

void interactive(){
    int n;
    cin >> n;
    vector<ull> numbers(n);
    for (int i = 0; i < n; ++i) {
        cin >> numbers[i];
    }

    cout << MaxPairwiseProductFast(numbers) << endl;
}

int main() {
    // StressTest(5, 10);
    interactive();
    return 0;
}
