#include <iostream>

using namespace std;


uint16_t get_fibonacci_mod(long long n, uint16_t m) {
    if (n <= 1) return n;

    uint16_t last2 = 0, last1 = 1;
    for (long long i = 0; i < n - 1; ++i) {
        uint16_t fib_reminder = (last2 + last1) % m;
        last2 = last1;
        last1 = fib_reminder;
    }

    return last1;
}

int fibonacci_mod_fast(long long n) {
    if(n <= 1) return n;
    n = n % 60; // 60 is the reminders period for modulo 10
    return get_fibonacci_mod(n, 10);
}

int fibonacci_sum_squares_fast(long long n) {
    int r1 = fibonacci_mod_fast(n);
    int r2 = fibonacci_mod_fast(n + 1);
    return (r1 * r2) % 10;
}

int fibonacci_sum_squares_naive(long long n) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current * current;
    }

    return sum % 10;
}

void stress_test(int n){
    for(int i = 0; i <=n; i++){
        int qsr1 = fibonacci_sum_squares_naive(i);
        int qsr2 = fibonacci_sum_squares_fast(i);
        if(qsr1 != qsr2){
            cout << "Square sum differs for n = " << i << endl << qsr1 << " " << qsr2 << endl;
            return;
        }
        cout << "OK n = " << i << " - " << qsr1 << " == " << qsr2 << endl;
    }
    cout << "All passed!" << endl;
}

void interactive(){
    long long n = 0;
    cin >> n;
    // cout << fibonacci_sum_squares_naive(n);
    cout << fibonacci_sum_squares_fast(n);
}

int main() {
    // stress_test(30);
    interactive();
    return 0;
}
