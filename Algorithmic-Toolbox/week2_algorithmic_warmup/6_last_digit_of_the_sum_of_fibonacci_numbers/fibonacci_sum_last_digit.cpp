#include <iostream>
#include <vector>

using namespace std;

uint16_t get_fibonacci_mod(long long n, uint16_t m) {
    if (n <= 1) return n;

    uint16_t last2 = 0, last1 = 1;
    for (long long i = 0; i < n - 1; ++i) {
        uint16_t fib_reminder = (last2 + last1) % m;
        last2 = last1;
        last1 = fib_reminder;
    }

    return last1;
}

int fibonacci_sum_naive(long long n) {
    if (n <= 1) return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current;
    }

    return sum % 10;
}

int fibonacci_sum_fast(long long n) {
    if(n <= 1) return n;
    n = n % 60; // 60 is the reminders period for modulo 10
    int sum_mod = get_fibonacci_mod(n + 2, 10);
    if(sum_mod == 0){
        sum_mod = 9;
    } else {
        sum_mod -= 1;
    }
    return sum_mod;
}

void stress_test(int n){
    for (int i = 0; i < n - 1; ++i) {
        int test1 = fibonacci_sum_naive(i);
        int test2 = fibonacci_sum_fast(i);
        if(test1 != test2){
            cout << "The 2 tests differ for N:" << endl;
            cout << i << endl;
            cout << "Naive: " << test1 << " - Fast: " << test2 << endl;
            return;
        } else {
            cout << test1 << " " << test2 << endl;
        }
    }
    cout << "All tests passed" << endl;
}

void interactive(){
    long long n = 0;
    cin >> n;
    // cout << fibonacci_sum_naive(n);
    cout << fibonacci_sum_fast(n);
}

int main() {
    interactive();
    // stress_test(50);
}
