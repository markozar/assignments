#include <iostream>
#include <vector>

using namespace std;

int get_fibonacci_partial_sum_naive(long long from, long long to) {
    long long sum = 0;

    long long current = 0;
    long long next  = 1;

    for (long long i = 0; i <= to; ++i) {
        if (i >= from) {
            sum += current;
        }

        long long new_current = next;
        next = next + current;
        current = new_current;
    }

    return sum % 10;
}

uint16_t get_fibonacci_mod(long long n, uint16_t m) {
    if (n <= 1) return n;

    uint16_t last2 = 0, last1 = 1;
    for (long long i = 0; i < n - 1; ++i) {
        uint16_t fib_reminder = (last2 + last1) % m;
        last2 = last1;
        last1 = fib_reminder;
    }

    return last1;
}

int fibonacci_sum_mod_fast(long long n) {
    if(n <= 1) return n;
    n = n % 60; // 60 is the reminders period for modulo 10
    int sum_mod = get_fibonacci_mod(n + 2, 10);
    if(sum_mod == 0){
        sum_mod = 9;
    } else {
        sum_mod -= 1;
    }
    return sum_mod;
}

int get_fibonacci_partial_sum_fast(long long from, long long to){
    if(from == 0) from = 1;
    int r_from = fibonacci_sum_mod_fast(from - 1);
    int r_to = fibonacci_sum_mod_fast(to);
    if(r_to < r_from) r_to += 10;
    return r_to - r_from;
}

void stress_test(int max_n){
    for(int m = 0; m <= max_n; m++){
        for(int n = m; n <= max_n; n++){
            int r1 = get_fibonacci_partial_sum_naive(m, n);
            int r2 = get_fibonacci_partial_sum_fast(m, n);
            if(r1 != r2){
                cout << "different partial sum mod for:" << endl << m << " " << n << endl;
                return;
            }
            cout << "OK [" << m << "," << n << "] " << r1 << " == " << r2 <<endl;
        }
    }
    cout << "All passed!" << endl;
}

void interactive(){
    long long from, to;
    cin >> from >> to;
    // cout << get_fibonacci_partial_sum_naive(from, to) << endl;
    cout << get_fibonacci_partial_sum_fast(from, to) << endl;
}

int main() {
    // stress_test(30);
    interactive();
}
