#include <iostream>

using namespace std;

int get_fibonacci_last_digit_naive(int n) {
    if (n <= 1)
        return n;

    int previous = 0;
    int current  = 1;

    for (int i = 0; i < n - 1; ++i) {
        int tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % 10;
}

int get_fibonacci_last_digit_minimal(int n) {
    if (n <= 1)
        return n;

    uint8_t previous = 0;
    uint8_t current  = 1;

    for (int i = 0; i < n - 1; ++i) {
        uint8_t tmp_previous = previous;
        previous = current;
        current = (tmp_previous + current) % 10;
    }

    return current;
}

void stress_test(int n){
    for (int i = 0; i < n - 1; ++i) {
        int test1 = get_fibonacci_last_digit_naive(i);
        int test2 = get_fibonacci_last_digit_minimal(i);
        if(test1 != test2){
            cout << "The 2 tests differ for N:" << endl;
            cout << i << endl;
            cout << "Naive: " << test1 << " - Minimal: " << test2 << endl;
            break;
        }
    }
    cout << "All tests passed" << endl;
}

void interactive(){
    int n;
    cin >> n;
    // int c = get_fibonacci_last_digit_naive(n);
    int c = get_fibonacci_last_digit_minimal(n);
    cout << c << endl;
}

int main() {
    interactive();
    // stress_test(30);
}
