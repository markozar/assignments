#include <iostream>
#include <cstdlib>

using namespace std;

long long lcm_naive(int a, int b) {
  for (long l = 1; l <= (long long) a * b; ++l)
    if (l % a == 0 && l % b == 0)
      return l;

  return (long long) a * b;
}

int gcd_fast(int a, int b){
  if(b == 0) return a;
  int reminder = a % b;
  return gcd_fast(b, reminder);
}

long long lcm_fast(int a, int b) {
  int gcd = gcd_fast(a, b);
  long long lcm = (a / gcd) * (long long) b;
  return lcm;
}

void stress_test(int max_n){
  int counter = 0, feedback_every_n = 10;
  while(true){
    int a = rand() % max_n + 1;
    int b = rand() % max_n + 1;
    // cout << "Computing naive..." << endl;
    long long lcm_s = lcm_naive(a, b);
    // cout << "Computing fast..." << endl;
    long long lcm_f = lcm_fast(a, b);
    if(lcm_s != lcm_f){
      cout << "The 2 tests differ in this input:" << endl;
      cout << "a = " << a << ", b = " << b << endl;
      cout << "Slow: " << lcm_s << " - Fast: " << lcm_f << endl;
      break;
    }
    if(++counter >= feedback_every_n){
      cout << counter << " tests were OK. Last test:" << endl;
      cout << "a = " << a << ", b = " << b << ", lcm = " << lcm_f << endl;
      counter -= feedback_every_n;
    }
  }
}

void interactive(){
  int a, b;
  cin >> a >> b;
  // cout << lcm_naive(a, b) << endl;
  cout << lcm_fast(a, b) << endl;
}

int main() {
  // stress_test(10000);
  interactive();
  return 0;
}
