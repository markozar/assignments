#include <iostream>
#include <cstdlib>

using namespace std;

int gcd_naive(int a, int b) {
  int current_gcd = 1;
  for (int d = 2; d <= a && d <= b; d++) {
    if (a % d == 0 && b % d == 0) {
      if (d > current_gcd) {
        current_gcd = d;
      }
    }
  }
  return current_gcd;
}

uint32_t gcd_fast(uint32_t a, uint32_t b){
  if(b == 0) return a;
  uint32_t reminder = a % b;
  return gcd_fast(b, reminder);
}

void stress_test(uint32_t max_n){
  int counter = 0, feedback_every_n = 400000;
  while(true){
    uint32_t n1 = rand() % max_n + 1;
    uint32_t n2 = rand() % max_n + 1;
    uint32_t test1 = gcd_naive(n1, n2);
    uint32_t test2 = gcd_fast(n1, n2);
    if(test1 != test2){
      cout << "The 2 tests differ in this input:" << endl;
      cout << n1 << ", " << n2 << endl;
      cout << "Slow: " << test1 << " - Fast: " << test2 << endl;
      break;
    }
    if(++counter >= feedback_every_n){
      cout << counter << " tests were OK. Last test:" << endl;
      cout << n1 << ", " << n2 << endl;
      counter -= feedback_every_n;
    }
  }
}

void interactive(){
  uint32_t a, b;
  cin >> a >> b;
  // cout << gcd_naive(a, b) << endl;
  cout << gcd_fast(a, b) << endl;
}

int main() {
  // stress_test(10000);
  interactive();
  return 0;
}
