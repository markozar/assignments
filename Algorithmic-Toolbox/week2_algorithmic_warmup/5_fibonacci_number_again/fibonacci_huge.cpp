#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

// Assumptions: the period exists and it is small
vector<uint16_t> find_period_for_mod(uint16_t m){
    vector<uint16_t> period; 
    if(m < 2){
        period.push_back(0);
        return period;
    }
    period.push_back(0);
    period.push_back(1);
    period.push_back(1);
    int repetition_index = 0;
    uint16_t last2 = 1, last1 = 1;
    while(repetition_index < period.size()){
        uint16_t fib_reminder = (last2 + last1) % m;
        last2 = last1;
        last1 = fib_reminder;
        if(fib_reminder == period[repetition_index]){
            repetition_index++;
        } else {
            if(repetition_index > 0){
                period.insert(period.end(), period.begin(), period.begin() + repetition_index);
                repetition_index = 0;
            }
            period.push_back(fib_reminder);
        }
    }
    return period;
}

long long get_fibonacci_huge_naive(long long n, long long m) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % m;
}

uint16_t get_fibonacci_huge_minimal(long long n, uint16_t m) {
    if (n <= 1) return n;

    uint16_t last2 = 0, last1 = 1;
    for (long long i = 0; i < n - 1; ++i) {
        uint16_t fib_reminder = (last2 + last1) % m;
        last2 = last1;
        last1 = fib_reminder;
    }

    return last1;
}

uint16_t get_fibonacci_huge_fast(long long n, uint16_t m) {
    if(n > 100){
        int period = find_period_for_mod(m).size();
        n = n % period;
    }
    return get_fibonacci_huge_minimal(n, m);
}

void stress_test(){
    int counter = 0, feedback_every_n = 1000;
    while(true) {
        long long n = rand() % 10000;
        uint16_t m = rand() % 100 + 2;
        // int r1 = (int) get_fibonacci_huge_naive(n, m);
        int r2 = (int) get_fibonacci_huge_minimal(n, m);
        int r3 = (int) get_fibonacci_huge_fast(n, m);
        if(r2 != r3){
            cout << "The tests differ for n, m:" << endl;
            cout << n << ", " << m << endl;
            cout << r2 << ", " << r3 << endl;
            break;
        }
        if(++counter >= feedback_every_n){
            cout << counter << " tests were OK." << endl;
            cout << n << ", " << m << endl;
            counter -= feedback_every_n;
        }
    }
}

void interactive(){
    long long n, m;
    cin >> n >> m;
    // cout << get_fibonacci_huge_naive(n, m) << endl;
    // cout << get_fibonacci_huge_minimal(n, m) << endl;
    cout << get_fibonacci_huge_fast(n, m) << endl;
}

void interactive_period_test(){
    while(true){
        int m;
        cout << "Find period for modul: ";
        cin >> m;
        vector<uint16_t> p = find_period_for_mod(m);
        cout << "Period size " << p.size() << ", periodic sequence:" << endl;
        for(int i = 0; i < p.size(); i++){
            cout << p[i] << " ";
        }
        cout << endl; 
    }
}

int main() {
    // stress_test();
    // interactive_period_test();
    interactive();
    return 0;
}
