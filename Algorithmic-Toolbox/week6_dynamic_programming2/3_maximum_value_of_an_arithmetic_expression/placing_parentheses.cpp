#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>

using std::vector;
using std::string;
using std::max;
using std::min;

class Table{
  public:
    Table(const string& str);
    ~Table();
    void min_max(size_t i, size_t j);
    size_t size();
    long long get_max();
  private:
    long long eval(long long a, long long b, char op);
    struct Expression{
      vector<long long> digits;
      vector<char> op;
    } m_exp;
    size_t m_size;
    long long** m_matrix;
};

Table::Table(const string& str){
  size_t symbol = 0;
  for(auto c = str.begin(); c != str.end(); c++){
    if(symbol++ % 2 == 0){  // is a digit
      m_exp.digits.push_back(std::atoll(&*c));
    } else{   // is an operator
      m_exp.op.push_back(*c);
    }
  }
  m_size = m_exp.digits.size();
  m_matrix = new long long*[m_size];
  for(int i = 0; i < m_size; ++i){
    m_matrix[i] = new long long[m_size];
    m_matrix[i][i] = m_exp.digits[i];
  }
}

Table::~Table(){
  for(int i = 0; i < m_size; ++i)
      delete [] m_matrix[i];
  delete [] m_matrix;
}

void Table::min_max(size_t i, size_t j){
  long long min = std::numeric_limits<long long>::max();
  long long max = std::numeric_limits<long long>::min();
  for(size_t k = i; k < j; k++){
    long long MM = eval(m_matrix[i][k], m_matrix[k+1][j], m_exp.op[k]);
    long long Mm = eval(m_matrix[i][k], m_matrix[j][k+1], m_exp.op[k]);
    long long mM = eval(m_matrix[k][i], m_matrix[k+1][j], m_exp.op[k]);
    long long mm = eval(m_matrix[k][i], m_matrix[j][k+1], m_exp.op[k]);
    max = std::max({max, MM, Mm, mM, mm});
    min = std::min({min, MM, Mm, mM, mm});
  }
  m_matrix[i][j] = max;
  m_matrix[j][i] = min;
}

size_t Table::size(){
  return m_size;
}

long long Table::get_max(){
  return m_matrix[0][m_size-1];
}

long long Table::eval(long long a, long long b, char op) {
  if (op == '*') {
    return a * b;
  } else if (op == '+') {
    return a + b;
  } else if (op == '-') {
    return a - b;
  } else {
    assert(0);
  }
}

long long get_maximum_value(const string &str) {
  Table tab(str);
  size_t n = tab.size();
  for(size_t s = 1; s < n; s++){
    for(size_t i = 0; i < n - s; i++){
      size_t j = i + s;
      tab.min_max(i, j); 
    }
  }
  return tab.get_max();
}

/* Input:
    5−8+7×4−8+9 (n=5, in general 2n + 1 symbols)
    1 <= n <= 14 (29 symbols max)
    digits: {0..9}
    operators: {+,-,* }

  Output:
    200 (maximum possible value)
*/
int main() {
  string s;
  std::cin >> s;
  std::cout << get_maximum_value(s) << std::endl;
}
