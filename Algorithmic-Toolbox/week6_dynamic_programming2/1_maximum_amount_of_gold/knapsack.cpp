#include <iostream>
#include <vector>
#include <algorithm>
#include <exception>

using std::vector;

int optimal_dp(int W, const vector<int> &w) {
  int weights = W + 1;
  int objects = w.size() + 1;
  vector<vector<int>> m(objects);
  for(auto& c: m) c.resize(weights);

  int obj_vw, val, row, col;

  for(row = 1; row < objects; row++){
    for(col = 1; col < weights; col++){
      val = m[row-1][col];
      obj_vw = w[row-1];
      if(obj_vw <= col){
        val = std::max(val, m[row-1][col - obj_vw] + obj_vw);
      }
      m[row][col] = val;
    }
  }
  return val;
}

int optimal_dp2(int W, const vector<int> &w) {
  int weights = W + 1;
  int objects = w.size() + 1;
  int** m = new int*[objects];    // 1.2x faster tha using vectors
  for (int i = 0; i < objects; ++i)
    m[i] = new int[weights];

  int obj_vw, val, row, col;
  for(col = 0; col < weights; col++) m[0][col] = 0;
  for(row = 1; row < objects; row++) m[row][0] = 0;

  for(row = 1; row < objects; row++){
    for(col = 1; col < weights; col++){
      val = m[row-1][col];
      obj_vw = w[row-1];
      if(obj_vw <= col){
        val = std::max(val, m[row-1][col - obj_vw] + obj_vw);
      }
      m[row][col] = val;
    }
  }
  for(int i = 0; i < objects; ++i)
      delete [] m[i];
  delete [] m;
  return val;
}


int optimal_weight(int W, const vector<int> &w) {
  //write your code here
  int current_weight = 0;
  for (size_t i = 0; i < w.size(); ++i) {
    if (current_weight + w[i] <= W) {
      current_weight += w[i];
    }
  }
  return current_weight;
}

void stress_test(){
  int32_t counter = 0, feedback_every_n = 1000;
  int W, objs;
  vector<int> w;
  try{
    while(true){
      int W = 1 + rand() % 10000;
      int objs =  1 + rand() % 300;
      w.resize(objs);
      for(auto& wi: w) wi = rand() % 100001;
      int res = optimal_dp2(W,w);
      if(++counter >= feedback_every_n){
        std::cout << counter << " tests were OK. Last test:" << std::endl;
        std::cout << W << " " << w.size() << " -> " << res << std::endl;
        counter -= feedback_every_n;
      }
    }
  } catch(const std::exception& e){
    std::cout << W << " " << w.size() << std::endl;
    for(const auto& wi: w) std::cout << wi << " ";
    std::cout << std::endl;
    return;
  }
}

/* Input:
  10 3    (W, n gold bars)
  1 4 8   (wi bar weights. Values = weight)

  Output:
  9       (max weight/value of gold that I can take)

  1 <= W <= 10^4 / 1 <= n <= 300 / 0 <= wi <= 10^5
*/
void interactive(){
  int n, W;
  std::cin >> W >> n;
  vector<int> w(n);
  for (auto& wi: w) std::cin >> wi;
  // std::cout << optimal_weight(W, w) << std::endl;
  std::cout << optimal_dp(W, w) << std::endl;
}

int main() {
  stress_test();
  // interactive();
  return 0;
}
