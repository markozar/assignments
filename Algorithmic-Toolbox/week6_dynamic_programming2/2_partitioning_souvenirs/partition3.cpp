#include <iostream>
#include <vector>
#include <numeric>

using std::vector;

int optimal_dp2(int W, const vector<int> &w) {
  int weights = W + 1;
  int objects = w.size() + 1;
  int** m = new int*[objects];    // 1.2x faster tha using vectors
  for (int i = 0; i < objects; ++i)
    m[i] = new int[weights];

  int obj_vw, val, row, col;
  for(col = 0; col < weights; col++) m[0][col] = 0;
  for(row = 1; row < objects; row++) m[row][0] = 0;

  for(row = 1; row < objects; row++){
    for(col = 1; col < weights; col++){
      val = m[row-1][col];
      obj_vw = w[row-1];
      if(obj_vw <= col){
        val = std::max(val, m[row-1][col - obj_vw] + obj_vw);
      }
      m[row][col] = val;
    }
  }
  int count = 0;
  for(row = 1; row < objects; row++){
    if(m[row][W] == W) count++;
  }

  for(int i = 0; i < objects; ++i)
      delete [] m[i];
  delete [] m;
  return count;
}

int partition3(vector<int> &A) {
  int num = A.size();
  int total_value = std::accumulate(A.begin(), A.end(), 0);
  if((total_value % 3) > 0) return 0;
  int third_value =  total_value / 3;
  int count = optimal_dp2(third_value, A);
  return (count >=3 ? 1 : 0);
}


void interactive(){
  int n;
  std::cin >> n;
  vector<int> A(n);
  for (size_t i = 0; i < A.size(); ++i) {
    std::cin >> A[i];
  }
  std::cout << partition3(A) << std::endl;
}

int main() {
  interactive();
}
