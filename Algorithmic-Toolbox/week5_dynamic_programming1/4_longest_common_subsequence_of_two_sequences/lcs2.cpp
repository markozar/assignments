#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using std::vector;

int lcs2(const vector<int> &a, const vector<int> &b) {
  size_t a_len = a.size();  //cols
  size_t b_len = b.size();  //rows
  size_t row, col;
  vector<vector<int>> sub_sequence_matrix(b_len + 1);
  for(auto& r: sub_sequence_matrix) r.resize(a_len + 1);
  int max_len;
  for(row = 1; row <= b_len; row++){
    for(col = 1; col <= a_len; col++){
      int sub_lenU = sub_sequence_matrix[row - 1][col];
      int sub_lenL = sub_sequence_matrix[row][col - 1];
      int sub_lenD = sub_sequence_matrix[row - 1][col - 1];
      if(a[col-1] == b[row-1]) sub_lenD++;
      max_len = std::max(sub_lenD, std::max(sub_lenU, sub_lenL));
      sub_sequence_matrix[row][col] = max_len;
    }
  }
  return max_len;
}

void stress_test(size_t max_sub_length, size_t range){
  srand(time(NULL));
  int counter = 0, feedback_every_n = 1000000 / (max_sub_length);
  size_t range_module = (range << 1) + 1;
  auto random_n = [range_module, range]() -> int {return ((rand() % range_module) - range);};
  vector<int> a, b, sub_seq;
  while(true){
    size_t sub_len = 1 + rand() % max_sub_length;
    sub_seq.resize(sub_len);
    // for(auto& s: sub_seq) s = (rand() % 2000000000) - 1000000000;
    for(auto& s: sub_seq) s = random_n();
    a.clear();
    auto it = sub_seq.begin();
    while(it != sub_seq.end()){
      if(rand() % 2){
        a.push_back(*it);
        it++;
      } else {
        int n = random_n();
        n = n >= 0 ? n + range + 1 : n - range;
        a.push_back(n);
      }
    }
    b.clear();
    it = sub_seq.begin();
    while(it != sub_seq.end()){
      if(rand() % 2){
        b.push_back(*it);
        it++;
      } else {
        int n = random_n();
        n = n >= 0 ? n + range*2 + 2 : n - range*2;
        b.push_back(n);
      }
    }
    int res = lcs2(a, b);
    if(res != sub_len){
      std::cout << "Subsequence length does not match!" << std::endl;
      std::cout << "lcs2: " << res << " - Expected: " << sub_len << std::endl;
      std::cout << "Sub sequence: ";
      for(auto s: sub_seq) std::cout << s << " ";
      std::cout << std::endl << a.size() << std::endl;
      for(auto ai: a) std::cout << ai << " ";
      std::cout << std::endl << b.size() << std::endl;
      for(auto bi: b) std::cout << bi << " ";
      std::cout << std::endl;
      return;
    }
    if(++counter >= feedback_every_n){
      std::cout << counter << " tests were OK. Last test:" << std::endl;
      std::cout << "lcs2: " << res << " - Expected: " << sub_len << std::endl;
      counter -= feedback_every_n;
    }
  }

}

/* Input:
    4         (n numbers in the first sequence)
    2 7 8 3   (a numbers of the first sequence)
    4         (m numbers in the first sequence)
    5 2 8 7   (b numbers of the second sequence)

  Output:
    2         (subsequence is 2,7 or 2,8)
  1 <= n,m <= 100
  -10^9 <= a, b <= 10^9
*/
void interactive(){
  size_t n;
  std::cin >> n;
  vector<int> a(n);
  for (auto& ai: a) std::cin >> ai;

  size_t m;
  std::cin >> m;
  vector<int> b(m);
  for (auto& bi: b) std::cin >> bi;

  std::cout << lcs2(a, b) << std::endl;
}

int main() {
  // stress_test(100, 1000000);
  interactive();
  return 0;
}
