#include <iostream>
#include <vector>
#include <limits>
// #include <algorithm>

using std::vector;

// denominators: 1, 3, 4 
int get_change(int m) {
  vector<int> coins = {1, 3, 4};
  vector<int> changes;  // we put all the changes in the vector, up to m, but actually we can do it keeping only 4 values (max of coins)
  //write your code here

  changes.push_back(0);
  for(int i = 1; i <= m; i++){
    int min_coin = std::numeric_limits<int>::max();
    for(auto c: coins){
      if((i - c) >= 0){
        min_coin = std::min(min_coin, changes[i - c]);
      } else {
        break;  // any other coins will be greater and will give negative result. Skip all of them.
      }
    }
    changes.push_back(min_coin + 1);
  }
  // Debug
  // for(auto c: changes) std::cout << c << " ";
  // std::cout << std::endl;
  // 
  return changes[m];
}


// m: money  1 <= m <= 10^3
// 2 : (2) 1+1
// 34: (9) 3 + 3 + 4 + 4 + 4 + 4 + 4 + 4 + 4
void interactive(){
  int m;
  std::cin >> m;
  std::cout << get_change(m) << '\n';
}


int main() {
  interactive();
  return 0;
}
