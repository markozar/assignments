#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

int edit_distance(const string &str1, const string &str2) {
  size_t str1_len = str1.size();
  size_t str2_len = str2.size();
  size_t row, col;
  // prepares the distance matrix. As the input length is low, we are going to use the
  // full matrix for simplicity. Otherwise we could use just 2 rows, as the only 2 rows
  // that we need each time are the last 2.
  vector<vector<int>> distance_matrix(str2_len + 1);
  for(auto& c: distance_matrix) c.resize(str1_len + 1);

  for(row = 0; row <= str2_len; row++){
    distance_matrix[row][0] = row;
  }
  for(col = 1; col <= str1_len; col++){
    distance_matrix[0][col] = col;
  }
  for(row = 1; row <= str2_len; row++){
    for(col = 1; col <= str1_len; col++){
      size_t insertion_cost = distance_matrix[row][col - 1] + 1;
      size_t deletion_cost = distance_matrix[row - 1][col] + 1;
      size_t substitution_cost = distance_matrix[row - 1][col - 1];
      if(str2[row - 1] != str1[col - 1]) substitution_cost++;
      distance_matrix[row][col] = std::min(insertion_cost, std::min(deletion_cost, substitution_cost));
    }
  }
  return distance_matrix[row - 1][col - 1];
}

/* Input:
    short   (string 1)
    ports   (string 2)

  Output:
    3       (edit distance)

  Characters in a string: 1 <= string <= 100 lower case
*/
void interactive(){
  string str1;
  string str2;
  std::cin >> str1 >> str2;
  std::cout << edit_distance(str1, str2) << std::endl;
}

int main() {
  interactive();
  return 0;
}
