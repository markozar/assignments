#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <limits>

using std::vector;
using std::pair;

struct op{
  int cost;
  int operand;
};

// Greedy. The safe move is not safe. It can be verified already for 10:
// greedy: ((((10/2)-1)/2)/2) => 4 operations
// DP: (((1*3)*3)+1) => 3 operations
vector<int> optimal_sequence(int n) {
  std::vector<int> sequence;
  while (n >= 1) {
    sequence.push_back(n);
    if (n % 3 == 0) {
      n /= 3;
    } else if (n % 2 == 0) {
      n /= 2;
    } else {
      n = n - 1;
    }
  }
  reverse(sequence.begin(), sequence.end());
  return sequence;
}

void optimal_sequence_dp(int n, vector<int>& sequence){
  if(n == 1){
    sequence.push_back(1);
    return;
  }

  // pair: (cost, number used for the operation). I.e:
  // 3 => (1, 1) cost 1, we used 1, because 3 = 3 * 1
  // 4 => (2, 3) cost 2, we used 3, because 4 = 3 + 1
  std::vector<op> dp_sequence;
  dp_sequence.push_back({0, 0});  // padding, so 1 is 1 based in the array and we avoid subtracting 1 from indexes
  dp_sequence.push_back({0, 0});
  int k = 2, index;
  op prev, best;
  do{
    prev = dp_sequence.back();
    best.cost = prev.cost;
    best.operand = k - 1;
    if(k % 2 == 0){
      index = k >> 1;
      prev = dp_sequence[index];
      if(prev.cost < best.cost){
        best.cost =prev.cost;
        best.operand = index;
      }
    }
    if(k % 3 == 0){
      index = k / 3;
      prev = dp_sequence[index];
      if(prev.cost < best.cost){
        best.cost =prev.cost;
        best.operand = index;
      }
    }
    best.cost++;
    dp_sequence.push_back(best);
  } while (k++ <= n);

  do{
    sequence.push_back(n);
    n = dp_sequence[n].operand;
  } while(n > 0);

  reverse(sequence.begin(), sequence.end());
}

void interactive(){
  int n;
  std::cin >> n;
  vector<int> sequence;
  optimal_sequence_dp(n, sequence);
  std::cout << sequence.size() - 1 << std::endl;
  for(auto s: sequence) std::cout << s << " ";
}

int main() {
  interactive();
  return 0;
}
