#include <iostream>
#include <vector>

using std::vector;
struct node{
  int len;
  bool match;
};

void lcs2(const vector<int> &a, const vector<int> &b, vector<int>& seq) {
  size_t a_len = a.size();  //cols
  size_t b_len = b.size();  //rows
  size_t row, col;
  vector<vector<node>> sub_sequence_matrix(b_len + 1);
  for(auto& r: sub_sequence_matrix) r.resize(a_len + 1);
  int max_len, sub_lenU, sub_lenL, sub_lenD;
  bool match;
  for(row = 1; row <= b_len; row++){
    for(col = 1; col <= a_len; col++){
      sub_lenU = sub_sequence_matrix[row - 1][col].len;
      sub_lenL = sub_sequence_matrix[row][col - 1].len;
      sub_lenD = sub_sequence_matrix[row - 1][col - 1].len;
      if(a[col-1] == b[row-1]){
        sub_lenD++;
        match = true;
      } else {
        match = false;
      }
      max_len = std::max(sub_lenD, std::max(sub_lenU, sub_lenL));
      sub_sequence_matrix[row][col] = {max_len, match};
    }
  }
  row--; col--;
  node& n = sub_sequence_matrix[row][col];
  while(seq.size() < max_len){
    if(n.match) seq.push_back(a[col-1]);
    node& d = sub_sequence_matrix[row-1][col-1];
    node& u = sub_sequence_matrix[row-1][col];
    node& l = sub_sequence_matrix[row][col-1];
    if(d.len >= u.len && d.len >= l.len){
      n = d; col--; row--;
    } else {
      if(l.len > u.len){
        n = l; col--;
      } else {
        n = u; row--;
      }
    }
  }
}

int lcs3(vector<int> &a, vector<int> &b, vector<int> &c) {
  size_t a_len = a.size();
  size_t b_len = b.size();
  size_t c_len = c.size();

  int sub_matrix[a_len +1][b_len +1][c_len +1];

  for(size_t i = 0; i <= a_len; i++){
    for(size_t j = 0; j <= b_len; j++){
      for(size_t k = 0; k <= c_len; k++){
        if(i == 0 || j == 0 || k == 0){ 
          sub_matrix[i][j][k] = 0;
        } else if(a[i-1] == b[j-1] && a[i-1] == c[k-1]){
          sub_matrix[i][j][k] = sub_matrix[i-1][j-1][k-1] +1;
        } else {
          sub_matrix[i][j][k] = std::max(sub_matrix[i-1][j][k], std::max(sub_matrix[i][j-1][k],sub_matrix[i][j][k-1]));
        }
      }
    }
  }
  return sub_matrix[a_len][b_len][c_len];
}

void interactive(){
  size_t an;
  std::cin >> an;
  vector<int> a(an);
  for(auto& ai: a) std::cin >> ai;
  size_t bn;
  std::cin >> bn;
  vector<int> b(bn);
  for(auto& bi: b) std::cin >> bi;
  size_t cn;
  std::cin >> cn;
  vector<int> c(cn);
  for(auto& ci: c) std::cin >> ci;
  std::cout << lcs3(a, b, c) << std::endl;
}

int main() {
  interactive();
  return 0;
}
