// Setup Visual Studio Code for Multi-File C++ Projects:
// https://dev.to/talhabalaj/setup-visual-studio-code-for-multi-file-c-projects-1jpi

// A simple representation of graph using STL
#include <iostream>
#include <vector>
#include <deque>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::deque;;

class Graph {
    friend class GraphBFS;
    public:
        Graph(uint16_t vertex_count);
        void addEdge(uint16_t v1, uint16_t v2);
        void printGraph() const;
        bool empty() const;
    private:
        uint16_t m_vertex_count, m_edge_count;
        vector<vector<uint16_t>> m_graph;
};

Graph::Graph(uint16_t vertex_count): m_vertex_count(vertex_count), m_edge_count(0) {
    m_graph.resize(vertex_count);
}

// No checking done on v1 and v2
void Graph::addEdge(uint16_t v1, uint16_t v2) {
    auto& adjacency_v1 = m_graph[v1];
    adjacency_v1.push_back(v2);
    auto& adjacency_v2 = m_graph[v2];
    adjacency_v2.push_back(v1);
    ++m_edge_count;
}

inline bool Graph::empty() const{
    return m_vertex_count == 0;
}
// A utility function to print the adjacency list
// representation of graph
void Graph::printGraph() const {
    uint16_t vertex_index = 0;
    for (auto& adjacency : m_graph) {
        cout << vertex_index++;
        for (auto vertex : adjacency) cout << "-> " << vertex;
        cout << endl;
    }
}

class GraphBFS {
 public:
    explicit GraphBFS(const Graph& graph);
    void begin();
    bool end();
    uint16_t next();
 private:
    const Graph& m_graph;
    vector<bool> m_visited;
    uint16_t m_visited_count;
    uint16_t m_enqueued_count;
    bool m_finished;
    deque<uint16_t> m_visit_queue;
};

GraphBFS::GraphBFS(const Graph& graph) : m_graph(graph),
                         m_visited(graph.m_vertex_count, false) {}

// begin starts always at vertex 0
void GraphBFS::begin() {
    m_finished = true;
    if (!m_graph.empty()) {
        fill(m_visited.begin(), m_visited.end(), false);
        m_visited_count = 0;
        m_visit_queue.push_back(0);
        m_enqueued_count = 1;
        m_finished = false;
    }
}

uint16_t GraphBFS::next() {
    uint16_t vertex = m_visit_queue.front();
    m_visit_queue.pop_front();
    m_visited[vertex] = true;
    ++m_visited_count;
    if (m_visited_count < m_graph.m_vertex_count) {
        if (m_enqueued_count < m_graph.m_vertex_count) {
            for (auto v : m_graph.m_graph[vertex]) {
                if (!m_visited[v]) {
                    m_visit_queue.push_back(v);
                    ++m_enqueued_count;
                }
            }
        }
    } else {
        m_finished = true;
    }
    return vertex;
}

inline bool GraphBFS::end() {
    return m_finished;
}

void doTest() {
    uint16_t vertex_count, edge_count;
    uint16_t vertex1, vertex2;
    cin >> vertex_count >> edge_count;
    Graph g(vertex_count);
    while (edge_count > 0) {
        cin >> vertex1 >> vertex2;
        g.addEdge(vertex1, vertex2);
        --edge_count;
    }
    cout << "=== Graph ====" << endl;
    g.printGraph();
    cout << "=== BFS ====" << endl;
    GraphBFS bfs(g);
    bfs.begin();
    while (!bfs.end()) {
        uint16_t v = bfs.next();
        cout << v << " ";
    }
    cout << endl;
}

// Driver code
int main() {
    uint16_t tests;
    cin >> tests;
    while (tests > 0) {
        doTest();
        --tests;
    }
    return 0;
}
