// Setup Visual Studio Code for Multi-File C++ Projects:
// https://dev.to/talhabalaj/setup-visual-studio-code-for-multi-file-c-projects-1jpi

// A simple representation of graph using STL
#include <iostream>
#include <vector>
#include <deque>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::deque;;

class Graph {
    friend class GraphDFS;
    public:
        Graph(uint16_t vertex_count);
        void addEdge(uint16_t v1, uint16_t v2, bool isDirectional = false);
        void printGraph() const;
        bool empty() const;
    private:
        uint16_t m_vertex_count;
        vector<vector<uint16_t>> m_graph;
};

Graph::Graph(uint16_t vertex_count): m_vertex_count(vertex_count) {
    m_graph.resize(vertex_count);
}

// No checking done on v1 and v2
void Graph::addEdge(uint16_t v1, uint16_t v2, bool isDirectional) {
    auto& adjacency_v1 = m_graph[v1];
    adjacency_v1.push_back(v2);
    if (!isDirectional && (v1 != v2)) {
        auto& adjacency_v2 = m_graph[v2];
        adjacency_v2.push_back(v1);
    }
}

inline bool Graph::empty() const{
    return m_vertex_count == 0;
}
// A utility function to print the adjacency list
// representation of graph
void Graph::printGraph() const {
    uint16_t vertex_index = 0;
    for (auto& adjacency : m_graph) {
        cout << vertex_index++;
        for (auto vertex : adjacency) cout << "-> " << vertex;
        cout << endl;
    }
}

class GraphDFS {
 public:
    explicit GraphDFS(const Graph& graph);
    void begin(uint16_t start_vertex = 0);
    bool end();
    uint16_t next();
 private:
    const Graph& m_graph;
    vector<bool> m_visited;
    uint16_t m_visited_count;
    uint16_t m_enqueued_count;
    bool m_finished;
    vector<uint16_t> m_visit_queue;
};

GraphDFS::GraphDFS(const Graph& graph) : m_graph(graph),
                         m_visited(graph.m_vertex_count, false) {}

// begin starts always at vertex 0
void GraphDFS::begin(uint16_t start_vertex) {
    m_finished = true;
    if (!m_graph.empty()) {
        fill(m_visited.begin(), m_visited.end(), false);
        m_visited_count = 0;
        m_visit_queue.push_back(start_vertex);
        m_enqueued_count = 1;
        m_finished = false;
    }
}

uint16_t GraphDFS::next() {
    uint16_t vertex = m_visit_queue.back();
    m_visit_queue.pop_back();
    m_visited[vertex] = true;
    ++m_visited_count;
    if (m_visited_count < m_graph.m_vertex_count) {
        if (m_enqueued_count < m_graph.m_vertex_count) {
            for (auto v : m_graph.m_graph[vertex]) {
                if (!m_visited[v]) {
                    m_visit_queue.push_back(v);
                    ++m_enqueued_count;
                }
            }
        }
    } else {
        m_finished = true;
    }
    return vertex;
}

inline bool GraphDFS::end() {
    return m_finished;
}

void doTest() {
    uint16_t vertex_count, edge_count;
    uint16_t vertex1, vertex2;
    bool directional;
    cin >> vertex_count >> edge_count;
    Graph g(vertex_count);
    while (edge_count > 0) {
        cin >> vertex1 >> vertex2 >> directional;
        g.addEdge(vertex1, vertex2, directional);
        --edge_count;
    }
    uint16_t start_vertex;
    cin >> start_vertex;
    cout << "=== Graph ====" << endl;
    g.printGraph();
    cout << "=== BFS ====" << endl;
    GraphDFS bfs(g);
    bfs.begin(start_vertex);
    while (!bfs.end()) {
        uint16_t v = bfs.next();
        cout << v << " ";
    }
    cout << endl;
}

/* Input format:
    1           (number of tests)
    4 6         (vertexes edges)
    0 1 0       (first edge: vertex1 vertex2 directional)
    0 2 0       (...)
    1 2 1
    1 3 1
    2 0 1
    3 3 1       (if vertex1 and vertex 2 are the same, directional can be either 0 or 1, it will always be added as a directional edge)
    2           (starting vertex)
*/
int main() {
    uint16_t tests;
    cin >> tests;
    while (tests > 0) {
        doTest();
        --tests;
    }
    return 0;
}
