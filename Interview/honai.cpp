#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int honaiMove(uint8_t n, char src, char dest, char aux) {
    static uint64_t moves = 0;
    if (n == 1) {
        cout << "Moving disk from " << src << " to " << dest << endl;
        ++moves;
    } else {
        honaiMove(n-1, src, aux, dest);
        cout << "Moving disk from " << src << " to " << dest << endl;
        ++moves;
        honaiMove(n-1, aux, dest, src);
    }
    return moves;
}


int main() {
    cout << "How many disks?" << endl;
    int disks;
    cin >> disks;
    honaiMove(disks, 'A', 'C', 'B');

    return 0;
}

