#include <iostream>
#include <vector>
#include <deque>

using std::cin;
using std::cout;
using std::vector;
using std::max;

// After 1 day of trials, I had to find a clue on internet. This contains several methods:
// Ref: https://www.geeksforgeeks.org/sliding-window-maximum-maximum-of-all-subarrays-of-size-k/
// Including the (now obvious...:) dequeue method that I adopted.

void max_sliding_window_fast(vector<int> const & A, int w) {
    std::deque<int> w_max;
    auto new_max = [&A, &w_max](int k){
        int val = A[k];
        while(!w_max.empty() && A[w_max.back()] <= val) w_max.pop_back();
        w_max.push_back(k);
    };
    for(int k = 0; k < w; k++){
        new_max(k);
    }
    cout << A[w_max.front()] << " ";
    for(int k = w; k < A.size(); k++){
        if(w_max.front() <= k - w) w_max.pop_front();
        new_max(k);
        cout << A[w_max.front()] << " ";
    }
    cout << std::endl;
    return;
}

void max_sliding_window_naive(vector<int> const & A, int w) {
    for (size_t i = 0; i < A.size() - w + 1; ++i) {
        int window_max = A.at(i);
        for (size_t j = i + 1; j < i + w; ++j)
            window_max = max(window_max, A.at(j));

        cout << window_max << " ";
    }
    cout << std::endl;
    return;
}

void interactive(){
    int n = 0;
    cin >> n;

    vector<int> A(n);
    for(auto& i: A) cin >> i;

    int w = 0;
    cin >> w;

    // max_sliding_window_naive(A, w);
    max_sliding_window_fast(A, w);
}

int main() {
    interactive();
    return 0;
}
