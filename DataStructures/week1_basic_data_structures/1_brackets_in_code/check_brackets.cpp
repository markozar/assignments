#include <iostream>
#include <stack>
#include <string>

struct Bracket {
    Bracket(char type, int position):
        type(type),
        position(position)
    {}

    bool Matchc(char c) const {
        if (type == '[' && c == ']')
            return true;
        if (type == '{' && c == '}')
            return true;
        if (type == '(' && c == ')')
            return true;
        return false;
    }

    char type;
    int position;
};
/* Input:
    string S which consists of big and small latin letters, digits, punctuation marks and brackets from the set []{}()
    1 <= S.size() <= 10^5
   
   Output:
    - "Success": brackets used correctly
    - 1-based index of the first unmatched closing bracket
    - 1-based index of the first unmatched opening bracket if no unmatched closing bracket
*/
void interactive(){
    std::string text;
    getline(std::cin, text);
    std::stack <Bracket> opening_brackets_stack;
    for (int i = 0; i < text.size(); i++) {
        char c = text[i];
        if (c == '(' || c == '[' || c == '{') {
            opening_brackets_stack.push({c, i});
        }
        if (c == ')' || c == ']' || c == '}') {
            if(opening_brackets_stack.empty()){
                opening_brackets_stack.push({c, i});
                break;
            }
            const Bracket& b = opening_brackets_stack.top();
            if(!b.Matchc(c)){
                opening_brackets_stack.push({c, i});
                break;
            }
            opening_brackets_stack.pop();
        }
    }
    if(opening_brackets_stack.empty()){
        std::cout << "Success" << std::endl;
    } else {
        std::cout << opening_brackets_stack.top().position + 1 << std::endl;
    }
}

int main() {
    interactive();
    return 0;
}
