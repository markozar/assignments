#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>

class Node {
public:
    Node():parent(nullptr) {}
    void setParent(Node *theParent) {
      parent = theParent;
      parent->children.push_back(this);
    }
    int level;
    Node *parent;
    std::vector<Node *> children;
};

int height_naive(const std::vector<Node>& nodes){
  // For each node walk back its parents. The longest chain will be the tree height.
  // Replace this code with a faster implementation
  int maxHeight = 0, n = nodes.size();
  for (int leaf_index = 0; leaf_index < n; leaf_index++) {
    int height = 0;
    for (const Node* v = &nodes[leaf_index]; v != NULL; v = v->parent)
      height++;
    maxHeight = std::max(maxHeight, height);
  }
  return maxHeight;
}

int height_fast(std::vector<Node>& nodes, Node* root){
  std::deque<Node*> bfs_tree;
  int level = root->level = 1;
  bfs_tree.push_back(root);
  while(!bfs_tree.empty()){
    Node* n = bfs_tree.back(); bfs_tree.pop_back();
    level = std::max(level, n->level);
    for(auto cn: n->children){
      cn->level = n->level + 1;
      bfs_tree.push_back(cn);
    }
  }
  return level;
}

/*  Input:
    n     (number of nodes: 1 <= n <= 10^5)
    i     (i integers : 0 <= i <= n-1, i: -1 => root, )

    Output:
    height of the tree
*/
void interactive(){
  int n;
  std::cin >> n;

  std::vector<Node> nodes;
  Node* root;
  nodes.resize(n);
  for (int child_index = 0; child_index < n; child_index++) {
    int parent_index;
    std::cin >> parent_index;
    if (parent_index >= 0){
      nodes[child_index].setParent(&nodes[parent_index]);
    } else {
      root = &nodes[child_index];
    }
  }
  // std::cout << height_naive(nodes) << std::endl;
  std::cout << height_fast(nodes, root) << std::endl;
}

int main (int argc, char **argv){
  interactive();
  return 0;
}
