#include <iostream>
#include <queue>
#include <vector>

struct Request {
    Request(int arrival_time, int process_time):
        arrival_time(arrival_time),
        process_time(process_time)
    {}

    int arrival_time;
    int process_time;
};

struct Response {
    Response(bool dropped, int start_time):
        dropped(dropped),
        start_time(start_time)
    {}

    bool dropped;
    int start_time;
};

class Buffer {
public:
    Buffer(int size):
        size_(size),
        finish_time_()
    {}

    Response Process(const Request &request) {
        if(finish_time_.empty()){
            finish_time_.push(request.arrival_time + request.process_time);
        } else if(request.arrival_time >= finish_time_.front()){
            finish_time_.pop();
            while(!finish_time_.empty() && request.arrival_time >= finish_time_.front()){
                finish_time_.pop();
            }
            if(finish_time_.empty()){
                finish_time_.push(request.arrival_time + request.process_time);
            } else {
                finish_time_.push(finish_time_.back() + request.process_time);
            }
        } else if(finish_time_.size() < size_){
            finish_time_.push(finish_time_.back() + request.process_time);
        } else {
            return {true, 0};
        }
        return {false, finish_time_.back() - request.process_time};
    }
private:
    int size_;
    std::queue <int> finish_time_;
};

std::vector <Request> ReadRequests() {
    std::vector <Request> requests;
    int count;
    std::cin >> count;
    for (int i = 0; i < count; ++i) {
        int arrival_time, process_time;
        std::cin >> arrival_time >> process_time;
        requests.push_back(Request(arrival_time, process_time));
    }
    return requests;
}

std::vector <Response> ProcessRequests(const std::vector <Request> &requests, Buffer *buffer) {
    std::vector <Response> responses;
    for (int i = 0; i < requests.size(); ++i)
        responses.push_back(buffer->Process(requests[i]));
    return responses;
}

void PrintResponses(const std::vector <Response> &responses) {
    for (const auto& r: responses) std::cout << (r.dropped ? -1 : r.start_time) << std::endl;
}
/* Input:
    S n     (S: buffer size, n: incoming network packets)
    Ai Pi   (Ai: Arrival time, Pi: Processing time)      

    1 <= S <= 10^5, 0 <= n <= 10^5, 0 <= Ai <= 10^6, 0 <= Pi <= 10^3.
    Ai <= A(i+1): it is guaranteed that the sequence of arrival times is non-decreasing
   
   Output:
    For each packet output either the moment of time (in milliseconds) when the processor
    began processing it or −1 if the packet was dropped (output the answers for the packets in the same
    order as the packets are given in the input).
    If there are no packets, you shouldn’t output anything.
*/
void interactive(){
    int size;
    std::cin >> size;
    std::vector <Request> requests = ReadRequests();

    Buffer buffer(size);
    std::vector <Response> responses = ProcessRequests(requests, &buffer);

    PrintResponses(responses);
}

int main() {
    interactive();
    return 0;
}
