#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>

using std::string;
using std::vector;
using std::cin;

struct Query {
    string type, name;
    int number;
};

struct Contact{
    Contact(string name_ = "", int number_ = -1) : number(number_), name(name_){};
    string name;
    int number;
    // bool operator == (const Contact& contact){
    //     return number == contact.number;
    // }
};

vector<Query> read_queries() {
    int n;
    cin >> n;
    vector<Query> queries(n);
    for (auto& query: queries) {
        cin >> query.type;
        if (query.type == "add")
            cin >> query.number >> query.name;
        else
            cin >> query.number;
    }
    return queries;
}

void write_responses(const vector<string>& result) {
    for (size_t i = 0; i < result.size(); ++i)
        std::cout << result[i] << "\n";
}

vector<string> process_queries_slow(const vector<Query>& queries) {
    vector<string> result;
    // Keep list of all existing (i.e. not deleted yet) contacts.
    vector<Contact> contacts;
    for (auto& query: queries)
        if (query.type == "add") {
            bool was_founded = false;
            // if we already have contact with such number,
            // we should rewrite contact's name
            for (size_t j = 0; j < contacts.size(); ++j)
                if (contacts[j].number == query.number) {
                    contacts[j].name = query.name;
                    was_founded = true;
                    break;
                }
            // otherwise, just add it
            if (!was_founded)
                contacts.push_back(Contact(query.name, query.number));
        } else if (query.type == "del") {
            for (size_t j = 0; j < contacts.size(); ++j)
                if (contacts[j].number == query.number) {
                    contacts.erase(contacts.begin() + j);
                    break;
                }
        } else {
            string response = "not found";
            for (size_t j = 0; j < contacts.size(); ++j)
                if (contacts[j].number == query.number) {
                    response = contacts[j].name;
                    break;
                }
            result.push_back(response);
        }
    return result;
}

vector<string> process_queries_fast(const vector<Query>& queries) {
    vector<string> result;
    vector<Contact> contacts(10000000);
    for (auto& query: queries){
        Contact& contact = contacts[query.number];
        if (query.type == "add") {
            contact.number = query.number;
            contact.name = query.name;
        } else if (query.type == "del") {
            contact.number = -1;
        } else {
            string response = "not found";
            if(contact.number == query.number){
                response = contact.name;
            }
            result.push_back(response);
        }
    }
    return result;
}

/*
Input example:
    3                   (1 < N <= 10^5)
    add 911 police
    find 911
    del 911

    Phone number: no more than 7 digits, no leading zeros.
    Name: at most 15 letters, non empty strings of latin letters.
    It’s guaranteed that there is no person with name “not found".
 */
int main() {
    write_responses(process_queries_fast(read_queries()));
    return 0;
}
