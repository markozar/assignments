#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

using std::string;
using std::vector;
using std::list;
using std::cin;

struct Query {
    string type, s;
    size_t ind;
};

class QueryProcessor {
    int m_bucket_count;
    // store all strings in one vector (slow solution)
    vector<string> m_elems;

    // hash data structure for the fast immplementation
    vector<list<string>> m_hash_set;

    int hash_func(const string& s) const {
        static const uint64_t multiplier = 263;
        static const uint64_t prime = 1000000007;
        uint64_t hash = 0;
        for (int i = static_cast<int> (s.size()) - 1; i >= 0; --i)
            hash = (hash * multiplier + s[i]) % prime;
        return hash % m_bucket_count;
    }

    public:
        explicit QueryProcessor(int bucket_count): m_bucket_count(bucket_count) {
            m_hash_set.resize(m_bucket_count);
        }

        Query readQuery() const {
            Query query;
            cin >> query.type;
            if (query.type != "check")
                cin >> query.s;
            else
                cin >> query.ind;
            return query;
        }

        void writeSearchResult(bool was_found) const {
            std::cout << (was_found ? "yes\n" : "no\n");
        }

        void processQuery(const Query& query) {
            if (query.type == "check") {
                // use reverse order, because we append strings to the end
                for (int i = static_cast<int>(m_elems.size()) - 1; i >= 0; --i)
                    if (hash_func(m_elems[i]) == query.ind)
                        std::cout << m_elems[i] << " ";
                std::cout << "\n";
            } else {
                vector<string>::iterator it = std::find(m_elems.begin(), m_elems.end(), query.s);
                if (query.type == "find")
                    writeSearchResult(it != m_elems.end());
                else if (query.type == "add") {
                    if (it == m_elems.end())
                        m_elems.push_back(query.s);
                } else if (query.type == "del") {
                    if (it != m_elems.end())
                        m_elems.erase(it);
                }
            }
        }

        void processQueryFast(const Query& query) {
            if (query.type == "check") {
                auto& chain = m_hash_set[query.ind];
                for(auto& e: chain) std::cout << e << " ";
                std::cout << std::endl;
            } else {
                int bucket = hash_func(query.s);
                auto& chain = m_hash_set[bucket];
                auto it = std::find(chain.begin(), chain.end(), query.s);
                if (query.type == "find")
                    writeSearchResult(it != chain.end());
                else if (query.type == "add") {
                    if (it == chain.end())
                        chain.push_front(query.s);
                } else if (query.type == "del") {
                    if (it != chain.end())
                        chain.erase(it);
                }
            }
        }

        void processQueries() {
            int n;
            cin >> n;
            for (int i = 0; i < n; ++i){
                // processQuery(readQuery());
                processQueryFast(readQuery());
            }
        }
};

int main() {
    std::ios_base::sync_with_stdio(false);  
    int bucket_count;
    cin >> bucket_count;
    QueryProcessor proc(bucket_count);
    proc.processQueries();
    return 0;
}
