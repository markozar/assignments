#include <iostream>
#include <vector>

using namespace std;

class Solver {
	static const uint64_t sc_x = 263;
	static const uint64_t sc_prime_m1 = 1000000007;
	static const uint64_t sc_prime_m2 = 1000000009;
	vector<uint32_t> m_hashes1;
	vector<uint32_t> m_hashes2;
	vector<uint32_t> m_x_poly1;
	vector<uint32_t> m_x_poly2;
	string m_s;

	void precompute_prefixes(){
		// Computing with prime 1
		int text_size = m_s.size();
		m_hashes1.resize(text_size +1);
		m_hashes1[0] = 0;
		for(int i = 0; i < text_size; ++i){
			m_hashes1[i+1] = (m_hashes1[i] * sc_x + m_s[i]) % sc_prime_m1;
		}
		m_x_poly1.resize(text_size);
		m_x_poly1[0] = 1;
		for(int i = 1; i < text_size; ++i){
			m_x_poly1[i] = (m_x_poly1[i-1] * sc_x) % sc_prime_m1;
		}

		// Computing with prime 2
		m_hashes2.resize(text_size +1);
		m_hashes2[0] = 0;
		for(int i = 0; i < text_size; ++i){
			m_hashes2[i+1] = (m_hashes2[i] * sc_x + m_s[i]) % sc_prime_m2;
		}
		m_x_poly2.resize(text_size);
		m_x_poly2[0] = 1;
		for(int i = 1; i < text_size; ++i){
			m_x_poly2[i] = (m_x_poly2[i-1] * sc_x) % sc_prime_m2;
		}
	}

public:	
	Solver(string s) : m_s(s) {
		precompute_prefixes();
	}
	bool ask_slow(int a, int b, int l) {
		return m_s.substr(a, l) == m_s.substr(b, l);
	}

	bool ask_fast(int a, int b, int l) {
		// s[a] .. s[a+l-1] with prime 1 and prime 2
		uint64_t ha1 = m_hashes1[a];
		if(ha1 != 0){
			ha1 = (m_x_poly1[l] * ha1) % sc_prime_m1;
		}
		uint64_t ha2 = m_hashes2[a];
		if(ha2 != 0){
			ha2 = (m_x_poly2[l] * ha2) % sc_prime_m2;
		}
		uint32_t Ha1 = (m_hashes1[a+l] + sc_prime_m1 - ha1) % sc_prime_m1;
		uint32_t Ha2 = (m_hashes2[a+l] + sc_prime_m2 - ha2) % sc_prime_m2;
		
		// s[b] .. s[b+l-1] with prime 1 and prime 2
		uint64_t hb1 = m_hashes1[b];
		if(hb1 != 0){
			hb1 = (m_x_poly1[l] * hb1) % sc_prime_m1;
		}
		uint64_t hb2 = m_hashes2[b];
		if(hb2 != 0){
			hb2 = (m_x_poly2[l] * hb2) % sc_prime_m2;
		}
		uint32_t Hb1 = (m_hashes1[b+l] + sc_prime_m1 - hb1) % sc_prime_m1;
		uint32_t Hb2 = (m_hashes2[b+l] + sc_prime_m2 - hb2) % sc_prime_m2;

		return (Ha1 == Hb1 && Ha2 == Hb2);
	}
};

/* Input:
	trololo		( 1 <= |S| <= 500'000 small latin letters)
	4			(q number of queries, 1<= q <= 100'000)
	0 0 7		(query 1: abl, 0<= a,b <= |S| - l. a & b are 0 based indexes)
	2 4 3
	3 5 1
	1 3 2

   Output:
	Yes | No for each query. "Yes" if S(a)..S(a+l-1) == S(b)..S(b+l-1), otherwise "No"
*/

int main() {
	ios_base::sync_with_stdio(0), cin.tie(0);

	string s;
	int q;
	cin >> s >> q;
	Solver solver(s);
	for (int i = 0; i < q; i++) {
		int a, b, l;
		cin >> a >> b >> l;
		cout << (solver.ask_fast(a, b, l) ? "Yes\n" : "No\n");
	}
}
