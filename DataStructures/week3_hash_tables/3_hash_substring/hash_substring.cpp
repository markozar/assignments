#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

typedef unsigned long long ull;

static const uint64_t multiplier = 263;
static const uint64_t prime = 1000000007;

struct Data {
    string pattern, text;
};

uint64_t hash_func(const string& s, int start, int len){
    uint64_t hash = 0;
    for (int i = start + len -1; i >= start; --i)
        hash = (hash * multiplier + s[i]) % prime;
    return hash;
}

Data read_input() {
    Data data;
    std::cin >> data.pattern >> data.text;
    return data;
}

void print_occurrences(const vector<int>& output) {
    for(auto i: output) std::cout << i << " ";
    std::cout << std::endl;
}

vector<int> get_occurrences_slow(const Data& input) {
    const string& s = input.pattern, t = input.text;
    vector<int> ans;
    for (size_t i = 0; i + s.size() <= t.size(); ++i)
        if (t.substr(i, s.size()) == s)
            ans.push_back(i);
    return ans;
}

vector<int> get_occurrences_fast(const Data& input) {
    int text_size = input.text.size();
    int pattern_size = input.pattern.size();

    // Calculates the precomputed hashes for the string using the Rabin Karp algorithm
    vector<uint64_t> precomputed(text_size - pattern_size + 1);
    precomputed[text_size-pattern_size] = hash_func(input.text, text_size - pattern_size, pattern_size);
    uint64_t xp = 1;
    for(int i = 1; i <= pattern_size; ++i){
        xp = (xp * multiplier) % prime;
    }
    for(int i = text_size - pattern_size -1; i >= 0; --i){
        uint64_t a = multiplier * precomputed[i+1] + input.text[i];
        uint64_t b = xp * input.text[i + pattern_size];
        if(a < b){
            // fix a - b in modulo arithmetic so that the result is positive
            precomputed[i] = (a + prime - (b % prime)) % prime;
        } else {
            precomputed[i] = (a - b) % prime;
        }
    }

    vector<int> ans;
    uint64_t pattern_hash = hash_func(input.pattern, 0, pattern_size);

    for(int i = 0; i <= text_size - pattern_size; ++i){
        if(pattern_hash != precomputed[i]) continue;
        if(input.text.compare(i, pattern_size, input.pattern) == 0){
            ans.push_back(i);
        }
    }

    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    // print_occurrences(get_occurrences_slow(read_input()));
    print_occurrences(get_occurrences_fast(read_input()));
    return 0;
}
