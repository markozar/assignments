#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

class Hasher {
	static const uint64_t sc_x = 263;
	static const uint64_t sc_prime_m1 = 1000000007;
	static const uint64_t sc_prime_m2 = 1000000009;

	struct HashedString{
		string s;
		vector<uint32_t> h1;
		vector<uint32_t> h2;
		vector<uint32_t> xp1;
		vector<uint32_t> xp2;
	};

	HashedString hsa, hsb;

	void precompute_prefixes(HashedString& hs){
		// Computing with prime 1
		int text_size = hs.s.size();
		hs.h1.resize(text_size +1);
		hs.h1[0] = 0;
		for(int i = 0; i < text_size; ++i){
			hs.h1[i+1] = (hs.h1[i] * sc_x + hs.s[i]) % sc_prime_m1;
		}
		hs.xp1.resize(text_size);
		hs.xp1[0] = 1;
		for(int i = 1; i < text_size; ++i){
			hs.xp1[i] = (hs.xp1[i-1] * sc_x) % sc_prime_m1;
		}

		// Computing with prime 2
		hs.h2.resize(text_size +1);
		hs.h2[0] = 0;
		for(int i = 0; i < text_size; ++i){
			hs.h2[i+1] = (hs.h2[i] * sc_x + hs.s[i]) % sc_prime_m2;
		}
		hs.xp2.resize(text_size);
		hs.xp2[0] = 1;
		for(int i = 1; i < text_size; ++i){
			hs.xp2[i] = (hs.xp2[i-1] * sc_x) % sc_prime_m2;
		}
	}

public:	
	Hasher(string s, string t) : hsa{s}, hsb{t} {
		precompute_prefixes(hsa);
		precompute_prefixes(hsb);
	}

	pair<uint32_t, uint32_t> get_a_hash(int index, int len) const{
		uint64_t ha1 = hsa.h1[index];
		if(ha1 != 0){
			ha1 = (hsa.xp1[len] * ha1) % sc_prime_m1;
		}
		uint64_t ha2 = hsa.h2[index];
		if(ha2 != 0){
			ha2 = (hsa.xp2[len] * ha2) % sc_prime_m2;
		}
		uint32_t Ha1 = (hsa.h1[index+len] + sc_prime_m1 - ha1) % sc_prime_m1;
		uint32_t Ha2 = (hsa.h2[index+len] + sc_prime_m2 - ha2) % sc_prime_m2;
		return {Ha1, Ha2};
	}

	pair<uint32_t, uint32_t> get_b_hash(int index, int len) const{
		uint64_t hb1 = hsb.h1[index];
		if(hb1 != 0){
			hb1 = (hsb.xp1[len] * hb1) % sc_prime_m1;
		}
		uint64_t hb2 = hsb.h2[index];
		if(hb2 != 0){
			hb2 = (hsb.xp2[len] * hb2) % sc_prime_m2;
		}
		uint32_t Hb1 = (hsb.h1[index+len] + sc_prime_m1 - hb1) % sc_prime_m1;
		uint32_t Hb2 = (hsb.h2[index+len] + sc_prime_m2 - hb2) % sc_prime_m2;
		return {Hb1, Hb2};
	}
};

struct Answer {
	size_t i, j, len;
};

struct HashValue{
	size_t index;
	pair<uint32_t, uint32_t> hashes;
};

Answer solve_slow(const string &s, const string &t) {
	Answer ans = {0, 0, 0};
	for (size_t i = 0; i < s.size(); i++)
		for (size_t j = 0; j < t.size(); j++)
			for (size_t len = 0; i + len <= s.size() && j + len <= t.size(); len++)
				if (len > ans.len && s.substr(i, len) == t.substr(j, len))
					ans = {i, j, len};
	return ans;
}

bool hash_compare(const HashValue& hva, const HashValue& hvb){
	return hva.hashes.first < hvb.hashes.first;
}

Answer solve_fast(const string &s, const string &t) {
	Hasher h(s, t);
	Answer ans = {0, 0, 0};
	size_t high = s.size() > t.size() ? t.size() : s.size();
	size_t low = 1;
	vector<HashValue> hashes_table_a(s.size());
	vector<HashValue> hashes_table_b(t.size());
	auto any_substring = [&](size_t len) -> bool{

		size_t max_index_a = s.size() - len;
		for (size_t i = 0; i <= max_index_a; ++i){
			hashes_table_a[i] = {i, h.get_a_hash(i, len)};
		}
		sort(hashes_table_a.begin(), hashes_table_a.begin() + max_index_a + 1, hash_compare);

		size_t max_index_b = t.size() - len;
		for (size_t i = 0; i <= max_index_b; ++i){
			hashes_table_b[i] = {i, h.get_b_hash(i, len)};
		}
		sort(hashes_table_b.begin(), hashes_table_b.begin() + max_index_b + 1, hash_compare);

		// Merge compare

		size_t a = 0, b = 0;
		while(a <= max_index_a && b <= max_index_b){
			HashValue& hva = hashes_table_a[a];
			HashValue& hvb = hashes_table_b[b];
			if(hva.hashes.first == hvb.hashes.first){
				if(hva.hashes.second == hvb.hashes.second){
					ans = {hva.index, hvb.index, len};
					return true;
				} else {
					++a; ++b;
				}
			} else {
				if(hva.hashes.first > hvb.hashes.first){
					++b;
				} else {
					++a;
				}
			}
		}
		return false;
	};
	while(low <= high){
		size_t len = (low + high) >> 1;
		if(any_substring(len)){
			low = len + 1;
		} else {
			high = len - 1;
		}
	}
	return ans;
}

int main() {
	ios_base::sync_with_stdio(false), cin.tie(0);
	string s, t;
	while (cin >> s >> t) {
		auto ans = solve_fast(s, t);
		cout << ans.i << " " << ans.j << " " << ans.len << "\n";
	}
}
