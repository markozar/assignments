#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::swap;
using std::pair;
using std::make_pair;

class HeapBuilder {
  private:
    vector<int> m_heap;
    int m_size;
    vector<pair<int, int>> m_swaps;

    void WriteHeap(){
      cout << "Heap:" << endl;
      for(const auto& n: m_heap) cout << n << " ";
      cout << endl;
    }

    void WriteResponse() const {
      cout << m_swaps.size() << endl;
      for(const auto& s: m_swaps) cout << s.first << " " << s.second << endl;
    }

    void ReadData() {
      int n;
      cin >> n;
      m_heap.resize(n);
      for(auto& ai: m_heap) cin >> ai;
      m_size = m_heap.size();
    }

    void GenerateSwaps() {
      m_swaps.clear();
      // The following naive implementation just sorts 
      // the given sequence using selection sort algorithm
      // and saves the resulting sequence of swaps.
      // This turns the given array into a heap, 
      // but in the worst case gives a quadratic number of swaps.
      //
      // TODO: replace by a more efficient implementation
      for (int i = 0; i < m_heap.size(); ++i)
        for (int j = i + 1; j < m_heap.size(); ++j) {
          if (m_heap[i] > m_heap[j]) {
            swap(m_heap[i], m_heap[j]);
            m_swaps.push_back(make_pair(i, j));
          }
        }
    }

    void MakeHeap() {
      if(m_size < 2) return; // already a heap
      for(int i = ((m_size-2) >> 1); i >= 0; i--){
        SwiftDown(i);
      }
    }

    void SwiftDown(int node){
      int min_node = node;
      int child = LeftChild(node);
      if(child < m_size && m_heap[child] < m_heap[min_node]){
        min_node = child;
      }
      child = RightChild(node);
      if(child < m_size && m_heap[child] < m_heap[min_node]){
        min_node = child;
      }
      if(min_node != node){
        swap(m_heap[node], m_heap[min_node]);
        m_swaps.push_back({node, min_node});
        SwiftDown(min_node);
      }
    }

    int LeftChild(int parent){
      return ((parent << 1) + 1);
    }
    int RightChild(int parent){
      return ((parent << 1) + 2);
    }

  public:
    void Solve() {
      ReadData();
      // GenerateSwaps();
      MakeHeap();
      WriteResponse();
      // WriteHeap();
    }
};

/*
  Input:
  5           (n 1 <= n <= 100000)
  1 3 2 4 5   (array of n distinct numbers. 0 <= ai <= 10^9)

  Output:
  3           (m total number of swaps, 0 <= m <= 4n)
  1 4         (m swaps, one per line)
  0 1
  1 3
*/

int main() {
  std::ios_base::sync_with_stdio(false);
  HeapBuilder m_heapbuilder;
  m_heapbuilder.Solve();
  return 0;
}
