#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::max;
using std::vector;

struct DisjointSetsElement {
	int size, parent, rank;
	
	DisjointSetsElement(int size = 0, int parent = -1, int rank = 0):
	    size(size), parent(parent), rank(rank) {}
};

struct DisjointSets {
	int size;
	int max_table_size;
	vector <DisjointSetsElement> sets;

	DisjointSets(int size): size(size), max_table_size(0), sets(size) {
		for (int i = 0; i < size; i++)
			sets[i].parent = i;
	}

	int getParent(int table) {
		int parent = sets[table].parent;
		if(parent != table){
			parent = getParent(parent);
			sets[table].parent = parent;
		}
		return parent;
	}

	void merge(int destination, int source) {
		int realDestination = getParent(destination);
		int realSource = getParent(source);
		if (realDestination != realSource) {
			auto& dest_table = sets[realDestination];
			auto& src_table = sets[realSource];
			if(dest_table.rank > src_table.rank){
				src_table.parent = realDestination;
				dest_table.size += src_table.size;
				max_table_size = max(max_table_size, dest_table.size);
			} else {
				dest_table.parent = realSource;
				src_table.size += dest_table.size;
				max_table_size = max(max_table_size, src_table.size);
				if(src_table.rank == dest_table.rank){
					src_table.rank++;
				}
			}
		}		
	}
};

/*
	Input:
	n m				(n: number of tables, m: number of merge queries to perform)
	r1 r2 .. rn		(ri: number of rows of table i)
	d1 s1			(di: destination table, si: source table)
	d2 s3
	..
	dm sm

	1 <= n,m <= 100000
	0 <= ri <= 10000

	Output: For each query print a line containing a single integer — the maximum of the sizes of all
			tables (in terms of the number of rows) after the corresponding operation

	Animated examples: https://www.cs.usfca.edu/~galles/visualization/DisjointSets.html
*/
int main() {
	int n, m;
	cin >> n >> m;

	DisjointSets tables(n);
	for (auto &table : tables.sets) {
		cin >> table.size;
		tables.max_table_size = max(tables.max_table_size, table.size);
	}

	for (int i = 0; i < m; i++) {
		int destination, source;
		cin >> destination >> source;
		--destination;
		--source;
		
		tables.merge(destination, source);
	        cout << tables.max_table_size << endl;
	}

	return 0;
}
