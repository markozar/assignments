#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::priority_queue;

class JobQueue {
  private:
    struct ScheduledJob{
      int thread;
      long long end_time;
      bool operator < (const ScheduledJob& scheduled_job) const{
        // < will be sorted as lower priority. To sort A before B => B < A
        if(scheduled_job.end_time == end_time){
          return scheduled_job.thread < thread;
        }
        return scheduled_job.end_time < end_time;
      }
    };
    
    int m_num_workers;
    vector<int> m_jobs;

    priority_queue<ScheduledJob> m_scheduled_jobs;

    vector<int> m_assigned_workers;
    vector<long long> m_start_times;

    void WriteResponse() const {
      for (int i = 0; i < m_jobs.size(); ++i) {
        cout << m_assigned_workers[i] << " " << m_start_times[i] << endl;
      }
    }

    void ReadData() {
      int m;
      cin >> m_num_workers >> m;
      m_jobs.resize(m);
      for(auto& j: m_jobs) cin >> j;
    }

    void AssignJobs() {
      // TODO: replace this code with a faster algorithm.
      m_assigned_workers.resize(m_jobs.size());
      m_start_times.resize(m_jobs.size());
      vector<long long> next_free_time(m_num_workers, 0);
      for (int i = 0; i < m_jobs.size(); ++i) {
        int duration = m_jobs[i];
        int next_worker = 0;
        for (int j = 0; j < m_num_workers; ++j) {
          if (next_free_time[j] < next_free_time[next_worker])
            next_worker = j;
        }
        m_assigned_workers[i] = next_worker;
        m_start_times[i] = next_free_time[next_worker];
        next_free_time[next_worker] += duration;
      }
    }

    void ScheduleJobs(){
      m_assigned_workers.resize(m_jobs.size());
      m_start_times.resize(m_jobs.size());
      // m_scheduled_jobs.resize(m_num_workers);
      if(m_num_workers >= m_jobs.size()){
        for(int job = 0; job < m_jobs.size(); job++){
          m_assigned_workers[job] = job;
          m_start_times[job] = 0;
        }
      } else {
        for(int thread = 0; thread < m_num_workers; thread++){
          m_assigned_workers[thread] = thread;
          m_start_times[thread] = 0;
          m_scheduled_jobs.push({thread, m_jobs[thread]});
        }
        for(int job = m_num_workers; job < m_jobs.size(); job++){
          ScheduledJob job_done = m_scheduled_jobs.top();
          m_scheduled_jobs.pop();
          m_scheduled_jobs.push({job_done.thread, m_jobs[job] + job_done.end_time});
          m_assigned_workers[job] = job_done.thread;
          m_start_times[job] = job_done.end_time;
        }
      }
    }

  public:
    void Solve() {
      ReadData();
      // AssignJobs();
      ScheduleJobs();
      WriteResponse();
    }
};

/*
  Input:
  2 5         (n m, n: threads, m: jobs, 1<= n,m <= 10^5)
  1 2 3 4 5   (m integers ti time to process the job, 0 <= ti <= 10^9)
*/
int main() {
  std::ios_base::sync_with_stdio(false);
  JobQueue job_queue;
  job_queue.Solve();
  return 0;
}
